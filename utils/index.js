import Loading from "./Loading";
import translator from "./translator";

const utils = {
  Loading,
  translator,
};

export default utils;
