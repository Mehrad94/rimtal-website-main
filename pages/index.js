import React, { useEffect } from "react";
import HomeScreen from "../website/screen/HomeScreen";
import * as sagaActions from "../store/actions/saga";
// import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";

const Home = (props) => {
  console.log({ MORI: props });

  // const dispatch = useDispatch();
  // const getHomeData = () => dispatch(sagaActions.getHomeData());
  useEffect(() => {
    props.dispatch(sagaActions.getHomeData());
    localStorage.setItem("DIR", "ltr");
  }, []);
  return <HomeScreen {...props} />;
};
Home.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};
export default connect()(Home);
