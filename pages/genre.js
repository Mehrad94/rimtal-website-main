import React from "react";
import GenreScreen from "../website/screen/GenreScreen";

const Genre = () => {
  return (
    <div className="row-container">
      <GenreScreen />
    </div>
  );
};

export default Genre;
