import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import MoodScreen from "../../panelAdmin/screen/Mood/MoodScreen";

const mood = () => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getMoodData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.MOOD));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getMoodData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchMoodData({ title, page }));
  };
  return <MoodScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default mood;
