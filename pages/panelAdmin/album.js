import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import AlbumScreen from "../../panelAdmin/screen/Album/AlbumScreen";

const album = (props) => {
  const { acceptedCardInfo, parentTrue } = props;
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const filters = panelAdmin.utils.consts.galleryConstants();
  console.log({ filters });

  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });

  useEffect(() => {
    dispatch(sagaActions.getGalleryData({ type: filters[0].value, page: 1 }));
    if (!parentTrue) dispatch(reduxActions.setPageName(panelAdmin.values.strings.GALLERY));
  }, []);

  const onDataChange = (filterName, page = 1) => {
    dispatch(sagaActions.getGalleryData({ type: filterName, page }));
  };

  return <AlbumScreen acceptedCardInfo={acceptedCardInfo} filters={filters} onDataChange={onDataChange} />;
};

export default album;
