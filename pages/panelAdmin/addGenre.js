import React, { useEffect } from "react";
import AddGenre from "../../panelAdmin/screen/Genres/AddGenre";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";

const addGenre = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_GENRE));
  }, []);
  return <AddGenre />;
};

export default addGenre;
