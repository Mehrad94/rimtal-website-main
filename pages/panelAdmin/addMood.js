import React, { useEffect } from "react";
import AddMood from "../../panelAdmin/screen/Mood/AddMood";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
const addMood = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  const sagaActions = panelAdmin.actions.sagaActions;
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_MOOD));
  }, []);
  return <AddMood />;
};

export default addMood;
