import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import CountryScreen from "../../panelAdmin/screen/Country/CountryScreen";

const country = () => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getCountryData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.COUNTRY));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getCountryData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchCountryData({ title, page }));
  };
  return <CountryScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default country;
