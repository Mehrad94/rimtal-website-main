import React, { useEffect, useMemo } from "react";
import ArtistScreen from "../../panelAdmin/screen/Artist/ArtistScreen";
import { useSelector, useDispatch } from "react-redux";
// import * as sagaActions from "../../store/actions/saga";
// import * as reduxActions from "../../store/actions/redux";
import panelAdmin from "../../panelAdmin";
import InstrumentScreen from "../../panelAdmin/screen/Instrument/InstrumentScreen";
const sagaActions = panelAdmin.actions.sagaActions;
const reduxActions = panelAdmin.actions.reduxActions;

const instrument = () => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getInstrumentData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.INSTRUMENT));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getInstrumentData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchInstrumentData({ title, page }));
  };
  return <InstrumentScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default instrument;
