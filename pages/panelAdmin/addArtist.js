import React, { useEffect } from "react";
import AddArtist from "../../panelAdmin/screen/Artist/AddArtist";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";

const addArtist = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  const sagaActions = panelAdmin.actions.sagaActions;
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_ARTIST));
  }, []);
  return <AddArtist />;
};

export default addArtist;
