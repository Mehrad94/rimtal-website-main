import React, { useEffect } from "react";
import AddCountry from "../../panelAdmin/screen/Country/AddCountry";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
const addCountry = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_COUNTRY));
  }, []);
  return <AddCountry />;
};

export default addCountry;
