import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import GenresScreen from "../../panelAdmin/screen/Genres/GenresScreen";

const genre = () => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getGenreData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.GENRES));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getGenreData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchGenreData({ title, page }));
  };
  return <GenresScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default genre;
