import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import MoodScreen from "../../panelAdmin/screen/Mood/MoodScreen";
import HashtagScreen from "../../panelAdmin/screen/Hashtag/HashtagScreen";

const hashtag = () => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getHashtagData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.HASHTAG));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getHashtagData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchHashtagData({ title, page }));
  };
  return <HashtagScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default hashtag;
