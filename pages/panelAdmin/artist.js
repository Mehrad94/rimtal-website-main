import React, { useEffect, useMemo } from "react";
import ArtistScreen from "../../panelAdmin/screen/Artist/ArtistScreen";
import { useSelector, useDispatch } from "react-redux";
// import * as sagaActions from "../../store/actions/saga";
// import * as reduxActions from "../../store/actions/redux";
import panelAdmin from "../../panelAdmin";
const sagaActions = panelAdmin.actions.sagaActions;
const reduxActions = panelAdmin.actions.reduxActions;

const artist = () => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(sagaActions.getArtistData({ page: 1 }));
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ARTIST));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    dispatch(sagaActions.getArtistData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchArtistData({ title, page }));
  };
  return <ArtistScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};

export default artist;
