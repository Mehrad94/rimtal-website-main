import React, { useEffect } from "react";
import panelAdmin from "../../panelAdmin";
import AddInstrument from "../../panelAdmin/screen/Instrument/AddInstrument";
import { useSelector, useDispatch } from "react-redux";
const addInstrument = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  const sagaActions = panelAdmin.actions.sagaActions;
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_INSTRUMENT));
  }, []);
  return <AddInstrument />;
};

export default addInstrument;
