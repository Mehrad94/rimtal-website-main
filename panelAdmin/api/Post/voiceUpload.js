import axios from "../axios-orders";
import axios2 from "axios";

import panelAdmin from "../..";

const voiceUpload = async (files, setLoading, type, setState, songName) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.strings;
  setLoading(true);
  console.log({ files, setLoading, type, setState, songName });

  // ============================================= const
  const CancelToken = axios2.CancelToken;
  const source = CancelToken.source();

  const settings = {
    onUploadProgress: (progressEvent) => {
      let percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
      );
      setState((prev) => ({ ...prev, progressPercentSongs: percentCompleted }));
    },
    cancelToken: source.token,
  };
  const formData = new FormData();
  formData.append("songName", songName);
  formData.append("song", files);

  const URL = strings.ApiString.SONG_UPLOAD;
  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
  // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  console.log({ formData });
  //console.log({ update });

  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios
  return axios
    .post(URL, formData, settings)
    .then((Response) => {
      console.log({ Response });
      setLoading(false);

      // update[name] = Response.data.voiceUrl;
      // return Response.data.songUrl;
      return true;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error")
        toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default voiceUpload;
