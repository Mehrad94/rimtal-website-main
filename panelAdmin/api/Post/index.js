import imageUpload from "./imageUpload";
// import owner from "./owner";
// import category from "./category";
// import slider from "./slider";
// import login from "./login";
// import album from "./album";
// import club from "./club";
// import banner from "./banner";
import artist from "./artist";
import voiceUpload from "./voiceUpload";
import videoUpload from "./videoUpload";
import genres from "./genres";
import country from "./country";
import instrument from "./instrument";
import mood from "./mood";
import hashtag from "./hashtag";
// import gallery from "./gallery";

const post = {
  imageUpload,
  // owner,
  // category,
  // slider,
  // login,
  // album,
  // club,
  // banner,
  artist,
  voiceUpload,
  videoUpload,
  genres,
  country,
  instrument,
  // gallery,
  mood,
  hashtag,
};
export default post;
