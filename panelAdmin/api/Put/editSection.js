import axios from "../axios-orders";

import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const editSection = async (param, setLoading) => {
  let URL = Strings.ApiString.SECTION;
  //console.log({ apiParam: param });
  // setLoading(true);

  return axios
    .put(URL + "/" + param._id, param.data)
    .then(Response => {
      //console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch(error => {
      //console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default editSection;
