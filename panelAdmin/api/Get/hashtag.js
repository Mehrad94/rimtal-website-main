import axios from "../axios-orders";
import panelAdmin from "../..";

const hashtag = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.HASHTAG;
  return axios.get(strings + "/" + page);
  // .then((hashtag) => {
  //   console.log({ hashtag });
  //   returnData(hashtag.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default hashtag;
