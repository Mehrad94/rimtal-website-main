import axios from "../axios-orders";

const gallery = async (returnData, page, type, loading) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.strings;
  return axios
    .get(Strings.ApiString.GALLERY + "/" + type + "/" + page)
    .then((gallery) => {
      console.log({ gallery });
      returnData(gallery.data);
      loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error")
        toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default gallery;
