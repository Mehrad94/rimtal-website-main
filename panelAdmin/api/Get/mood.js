import axios from "../axios-orders";
import panelAdmin from "../..";

const mood = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.MOOD;
  return axios.get(strings + "/" + page);
  // .then((mood) => {
  //   console.log({ mood });
  //   returnData(mood.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default mood;
