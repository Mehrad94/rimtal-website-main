import axios from "../axios-orders";
import panelAdmin from "../..";

const countrySearch = async (title, page, returnData) => {
  console.log({ title, page });
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.COUNTRY;
  return axios
    .get(strings + "/" + title + "/" + page)
    .then((countrySearch) => {
      // console.log({ countrySearch });
      returnData(countrySearch.data);
      // loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default countrySearch;
