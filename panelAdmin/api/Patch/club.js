import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const club = async (param, setLoading) => {
  let URL = Strings.ApiString.CLUB;
  console.log({ apiParam: param });
  return axios
    .patch(URL + "/" + param._id, param.data)
    .then(Response => {
      console.log({ Response });
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch(error => {
      console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default club;
