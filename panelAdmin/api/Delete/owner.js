import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import panelAdmin from "../..";
const toastify = panelAdmin.utils.toastify;
const owner = async (param) => {
  let URL = Strings.ApiString.OWNER;
  return axios
    .delete(URL + "/" + param)
    .then((Response) => {
      console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });

      if (error.message === "Network Error")
        toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default owner;
