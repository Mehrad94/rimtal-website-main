const ADMIN = "/admin";
const SONG_UPLOAD = ADMIN + "/songupload";
const UPLOAD = ADMIN + "/upload";
const ALBUM = ADMIN + "/album";
const LOGIN = ADMIN + "/login";
const IMAGE_UPLOAD = ADMIN + "/upload/image";
const GENRES = ADMIN + "/genre";
const ARTIST = ADMIN + "/artist";
const ARTIST_SEARCH = ARTIST + "/search";
const COUNTRY = ADMIN + "/country";
const INSTRUMENT = ADMIN + "/instrument";
const GALLERY = ADMIN + "/gallery";
const SONG = ADMIN + "/songlibrary";
const MOOD = ADMIN + "/mood";
const HASHTAG = ADMIN + "/hashtag";
const apiString = {
  SONG_UPLOAD,
  ALBUM,
  LOGIN,
  UPLOAD,
  IMAGE_UPLOAD,
  GENRES,
  ARTIST_SEARCH,
  COUNTRY,
  INSTRUMENT,
  GALLERY,
  SONG,
  ARTIST,
  MOOD,
  HASHTAG,
};
export default apiString;
