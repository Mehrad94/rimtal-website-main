const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";

const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";

const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";

const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";

const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";

const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";

const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";

const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";

const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD,
};
export default sideMenu;
