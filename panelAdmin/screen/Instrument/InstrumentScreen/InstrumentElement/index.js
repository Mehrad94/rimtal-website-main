import React from "react";
// import table from "../../../../util/consts/table";
import Pageination from "../../../../component/UI/Pagination";
import InputVsIcon from "../../../../component/UI/InputVsIcon";
import TabelBasic from "../../../../component/UI/Tables";
import panelAdmin from "../../../..";
// import IsNull from "../../../../components/UI/IsNull";
const table = panelAdmin.utils.consts.table;
const InstrumentElement = (props) => {
  const { Instrument, handelPage, tableOnclick, handelchange } = props;
  // console.log({ Instrument, handelPage, tableOnclick, handelchange });

  return (
    Instrument && (
      <div className="countainer-main centerAll ">
        <div className="elemnts-box  boxShadow tableMainStyle">
          {/* <TabelBasic
            subTitle={
              <div className="disColum ">
                <h4 style={{ color: "black", paddingBottom: "0.2em", fontSize: "1.5em" }}>{" ساز ها"}</h4>
                {<span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${Instrument.total}  `}</span>}
              </div>
            }
            btnHead={false}
            imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
            tbody={table.instrument(Instrument.docs).tbody}
            thead={table.instrument(Instrument.docs).thead}
            onClick={tableOnclick}
            inputRef
            searchHead={InputVsIcon({
              name: "genreTitle",
              icon: "far fa-search",
              placeholder: "Violin , Piano ...",
              onChange: handelchange,
              dir: "ltr",
            })}
          /> */}

          {Instrument && Instrument.pages >= 2 && (
            <Pageination
              limited={"3"}
              pages={Instrument ? Instrument.pages : ""}
              activePage={Instrument ? Instrument.page : ""}
              onClick={handelPage}
            />
          )}
        </div>
      </div>
    )
  );
};

export default InstrumentElement;
