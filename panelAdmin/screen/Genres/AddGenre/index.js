import React, { useState } from "react";
import { post } from "../../../api";
import FormInputGenres from "./FormInputGenres";
import panelAdmin from "../../..";

const AddGenre = () => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addGenres });
  const [state, setState] = useState({ progressPercentImage: null, progressPercentSongs: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [imageAccepted, setImageAccepted] = useState({ web: null, phone: null });

  const [checkSubmited, setCheckSubmited] = useState(false);

  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (await post.genres(formData)) setData({ ...states.addGenres });
  };
  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "albumPic" });
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputGenres
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{"افزودن ژانر جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              {" افزودن"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddGenre;
