import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post } from "../../../api";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import FormInputAlbum from "./FormInputAlbum";
import panelAdmin from "../../..";

const states = panelAdmin.utils.consts.states;
const onChanges = panelAdmin.utils.onChanges;

const AddAlbum = () => {
  const [data, setData] = useState({ ...states.addAlbum });
  const [state, setState] = useState({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: { value: "", name: "" },
  });
  const [Loading, setLoading] = useState(false);
  const [Modal, setModal] = useState({
    show: false,
  });
  const [checkSubmited, setCheckSubmited] = useState(false);
  // ======================================== modal
  const onHideModal = () => setModal({ ...Modal, show: false });

  const onShowlModal = () => setModal({ ...Modal, show: true });

  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form)
      formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    // const initialStateTermsOfUse = updateObject(states.addAlbum.Form["termsOfUse"], { value: [] });
    // const initialStateFeatures = updateObject(states.addAlbum.Form["features"], { value: [] });
    // const updatedForm = updateObject(states.addAlbum.Form, { ["TermsOfUse"]: initialStateTermsOfUse, ["features"]: initialStateFeatures });
    if (await post.album(formData)) setData({ ...states.addAlbum });
  };
  // ========================= End submited =================
  // ============================= remove
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value)
      inputChangedHandler({
        name: state.remove.name,
        value: state.remove.value,
      });
  };
  const removeHandel = (value, name) => {
    onShowlModal();
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const inputChangedHandler = async (event) =>
    await onChanges.globalChange({
      event,
      data,
      setData,
      setState,
      setLoading,
      imageType: "albumPic",
    });

  const stateArray = [];
  for (let key in data.Form)
    stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputAlbum
      removeHandel={removeHandel}
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
    />
  );
  return (
    <div className="countainer-main centerAll formFlex">
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        <ModalTrueFalse
          modalHeadline={"آیا مطمئن به حذف آن هستید !"}
          modalAcceptTitle={"بله"}
          modalCanselTitle={"خیر"}
          modalAccept={__returnPrevstep}
        />
      </ModalBox>
      <div className="form-countainer">
        <div className="form-subtitle">افزودن آلبوم جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button
              className="btns btns-primary"
              disabled={!data.formIsValid}
              onClick={_onSubmited}
            >
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddAlbum;
