import React, { useState, useEffect } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
const FormInputAlbum = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;

  const [SearchGender, setSearchGender] = useState(false);
  const [Genres, setGenres] = useState(false);
  const setloading = (param) => {};
  useEffect(() => {
    getApi();
  }, []);
  const getApi = (page = "1") => {
    get.genres(setGenres, page, setloading);
  };
  console.log({ Genres });

  let genresData = [];
  if (Genres && Genres.docs.length) for (const index in Genres.docs) genresData.push({ value: Genres.docs[index]._id, title: Genres.docs[index].titleFa, description: Genres.docs[index].titleEn });

  useEffect(() => {
    setSearchGender([]);
  }, [checkSubmited]);
  let SearchGenderData = [];
  for (const index in SearchGender) {
    let nameFa = SearchGender[index].nameFa;
    let nameEn = SearchGender[index].nameEn;
    let descriptionFa = SearchGender[index].descriptionFa;
    let descriptionEn = SearchGender[index].descriptionEn;
    let name = nameFa ? nameFa : nameEn;
    let description = descriptionFa ? descriptionFa : descriptionEn;
    SearchGenderData.push({ value: SearchGender[index]._id, title: name, description: description, image: SearchGender[index].avatar });
  }
  console.log({ SearchGender, SearchGenderData });

  const searchedGender = (e) => {
    if (e.currentTarget.value.length >= 2) get.artistSearch(e.currentTarget.value, setSearchGender);
  };
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress, dropDownData;

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");

        if (formElement.id === "songs") {
          progress = state.progressPercentSongs;
        } else if (formElement.id === "image") {
          progress = state.progressPercentImage;
        }
        if (formElement.id === "artist") {
          changed = searchedGender;
          dropDownData = SearchGenderData;
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
        }
        if (formElement.id === "genres") {
          dropDownData = genresData;
        }
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputAlbum;
