import React, { useState } from "react";
import SideMenu from "../component/SideMenu";
import Header from "../component/Header";
import BackgrandCover from "../component/UI/BackgrandCover";
import { ToastContainer, toast } from "react-toastify";
import withRedux from "next-redux-wrapper";
import { Provider } from "react-redux";
import Scrollbar from "react-scrollbars-custom";

const PanelScreen = (props) => {
  const { router } = props;
  const [sidebarToggle, setSidebarToggle] = useState(false);

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };
  return (
    <div className={`panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`}>
      <div style={{ direction: "rtl", display: "flex" }}>
        <SideMenu windowLocation={router.asPath} />
        <div className="panelAdmin-container">
          <Header _handelSidebarToggle={_handelSidebarToggle} />
          <Scrollbar style={{ width: "100%", height: " 100% " }}>
            <div className="panelAdmin-content">{props.children}</div>
          </Scrollbar>
        </div>
      </div>
      <BackgrandCover fadeIn={sidebarToggle} onClick={_handelSidebarToggle} />
      <ToastContainer rtl />
    </div>
  );
};

export default PanelScreen;
