import React, { useRef, useEffect, useState, Fragment } from "react";
import { post } from "../../../api";
import FormInputArtist from "./FormInputArtist";
import GalleryScreen from "../../Gallery/GalleryScreen";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";
import updateObject from "../../../utils/updateObject";
const AddArtist = () => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addArtist });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
  });
  const [imageAccepted, setImageAccepted] = useState({ web: null, phone: null });

  const [checkSubmitted, setCheckSubmitted] = useState(false);
  // ======================================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    // handelgetApi();
    setEditData();
  };
  // console.log(ModalInpts);

  const onShowModal = (event) => {
    if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: null });
    else if (event ? event.kindOf === "showGallery" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: event.name });
  };
  // ============================= remove
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted.web === images.web ? setImageAccepted({ web: null, phone: null }) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "avatar", value: imageAccepted });
  };
  const renderModalInputs = (
    <div className={"width80"}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {
          ModalInpts.kindOf === "showGallery" && (
            <Gallery
              // clickedParent={(value) => inputChangedHandler({ name: ModalInpts.name, value })}
              parentTrue
              acceptedCardInfo={{ handelAcceptedImage: acceptedImage, acceptedCard: imageAccepted }}
            />
          )
          // <ShowListData data={""}/>
        }
        {/* {ModalInpts.kindOf === "showGallery" && <ShowListData getApi={handelgetApi} InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />} */}
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />}
        {ModalInpts.kindOf === "question" ? (
          ""
        ) : (
          <div className="btns-container">
            <button disabled={!imageAccepted.web} className="btns btns-primary" onClick={(e) => acceptedImageFinal(e)}>
              تایید{" "}
            </button>
            <button className="btns btns-warning" onClick={onHideModal}>
              بستن{" "}
            </button>
          </div>
        )}
      </ModalBox>
    </div>
  );

  // ========================= End modal =================
  // ============================= submited
  const _onSubmitted = async (e) => {
    setCheckSubmitted(!checkSubmitted);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    const initialStateBirthday = updateObject(states.addArtist.Form["birthday"], { value: { month: "", day: "", year: "" } });
    // const initialStateFeatures = updateObject(states.addArtist.Form["features"], { value: [] });
    const birthDay = formData.birthday;
    if (parseInt(birthDay.day) < 10) formData.birthday.day = "0" + formData.birthday.day;
    if (parseInt(birthDay.month) < 10) formData.birthday.month = "0" + formData.birthday.month;
    console.log({ formData });

    const updatedForm = updateObject({ ...states.addArtist.Form }, { ["birthday"]: initialStateBirthday });
    if (await post.artist(formData)) setData({ Form: updatedForm, formIsValid: false });
  };
  console.log({ moj1: states.addArtist, moj2: data });

  // ========================= End submited =================

  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "thumbnail" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputArtist removeHandel={removeHandel} _onSubmited={_onSubmitted} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmitted} showModal={onShowModal} />;
  return (
    <div className="countainer-main  ">
      {renderModalInputs}
      <div style={{ marginTop: "20px" }} className="form-countainer">
        <div className="form-subtitle">افزودن هنرمند جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmitted}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddArtist;
