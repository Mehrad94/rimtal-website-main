import React, { useEffect, useState, Fragment, useRef } from "react";
import AddGallery from "../AddGallery";
import { Modal, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import panelAdmin from "../../..";
import { connect } from "react-redux";
import _ from "lodash";
import MyVerticallyCenteredModal from "../../../component/UI/Modals/MyVerticallyCenteredModal";
import GalleryImage from "./GalleryImage";
import GalleryFilter from "./GalleryFilter";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";

const GalleryScreen = (props) => {
  const { onDataChange, filters, acceptedCardInfo } = props;
  const states = panelAdmin.utils.consts.states;
  const [showUploadModal, setUploadModal] = useState(false);
  const [currentFilter, setCurrentFilter] = useState("album");
  const [currentPage, setCurrentPage] = useState(1);
  const [data, setData] = useState({ ...states.addGallery });

  const _onSubmit = async (e) => {
    // setCheckSubmited(!checkSubmited);
    // e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    onDataChange(currentFilter, currentPage);
    setData({ ...states.addGallery });
  };
  const store = useSelector((state) => {
    return state.gallery;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const galleryData = store.galleryData;
  const searchGalleryData = store.searchGalleryData;
  const GallerySearch = searchGalleryData ? (searchGalleryData.docs.length ? true : false) : false;
  let dropDownData = [];
  for (const index in filters)
    dropDownData.push({
      value: filters[index].value,
      title: filters[index].title,
    });

  console.log({ storeGalleryScreen: store });
  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: "ثبت",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit,
  };

  const onFilterClick = (filterName, currentPage) => {
    setCurrentFilter(filterName);
    onDataChange(filterName, currentPage);
  };

  const AddImage = () => {
    return (
      <Button onClick={() => setUploadModal(true)} className="">
        {"افزودن عکس"}
      </Button>
    );
  };
  console.log({ galleryData });

  const showDataElement = galleryData.docs && galleryData.docs.length ? <ShowCardInformation data={card.gallery(galleryData.docs, acceptedCardInfo ? acceptedCardInfo.acceptedCard : false)} onClick={null} optionClick={null} acceptedCardInfo={acceptedCardInfo} /> : "";
  //==============================================================================

  return (
    <div className="gallery">
      <MyVerticallyCenteredModal {...modalData}>
        <AddGallery data={data} setData={setData} />
      </MyVerticallyCenteredModal>
      <div className="gallery-header-wrapper">
        <div className="gallery-header">
          <GalleryFilter dropDownData={dropDownData} currentFilter={currentFilter} filters={filters} onFilterClick={onFilterClick} />
          <AddImage />
        </div>
        {showDataElement}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </div>
  );
};
export default GalleryScreen;
