import React from "react";
import GalleryImageCard from "../../../../component/cards/GalleryImageCard";
const GalleryImage = ({ galleryData, handelAcceptedImage, imageAccepted }) => {
  return (
    <div className="panel-main-container  ">
      {galleryData.docs &&
        galleryData.docs.map((info) => (
          <GalleryImageCard
            handelAcceptedImage={handelAcceptedImage}
            key={info._id}
            image={{ web: info.web, phone: info.phone }}
            title={info.title}
            imageAccepted={imageAccepted}
          />
        ))}
    </div>
  );
};
export default GalleryImage;
