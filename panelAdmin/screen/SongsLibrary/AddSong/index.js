import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import FormInputSong from "./FormInputSong";
import onChanges from "../../../util/onChanges";
import states from "../../../util/consts/states";
import { post } from "../../../api";
const AddSong = ({ onHideModal, getApi }) => {
  const [data, setData] = useState({ ...states.addSong });
  const [state, setState] = useState({ progressPercentImage: null, progressPercentSongs: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [Modal, setModal] = useState({
    show: false,
  });
  const [checkSubmited, setCheckSubmited] = useState(false);
  // ======================================== modal

  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    // const initialStateTermsOfUse = updateObject(states.addSong.Form["termsOfUse"], { value: [] });
    // const initialStateFeatures = updateObject(states.addSong.Form["features"], { value: [] });
    // const updatedForm = updateObject(states.addSong.Form, { ["TermsOfUse"]: initialStateTermsOfUse, ["features"]: initialStateFeatures });
    // if (await post.Song(formData))
    onHideModal();
    setData({ ...states.addSong });
  };
  // ========================= End submited =================
  // ============================= remove
  const __returnPrevstep = (value) => {
    // onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    // onShowlModal();
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const inputChangedHandler = async (event) => {
    await onChanges.globalChange({ event, data, setData, setState, setLoading, fileName: data.Form.songName.value });
  };
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputSong
      removeHandel={removeHandel}
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      </ModalBox>
      <div className="form-countainer">
        <div className="form-subtitle">افزودن آهنگ جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              تایید{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddSong;
