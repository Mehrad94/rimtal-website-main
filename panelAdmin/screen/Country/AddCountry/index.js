import React, { useRef, useEffect, useState, Fragment } from "react";
import { post } from "../../../api";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import FormInputCountry from "./FormInputCountry";
import GalleryScreen from "../../Gallery/GalleryScreen";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";
const AddCountry = () => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addCountry });
  const [state, setState] = useState({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: { value: "", name: "" },
  });
  const [Loading, setLoading] = useState(false);
  const [editData, setEditData] = useState(false);
  const [imageAccepted, setImageAccepted] = useState({ web: null, phone: null });

  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
  });
  const [checkSubmited, setCheckSubmited] = useState(false);
  // ======================================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    // handelgetApi();
    setEditData();
  };
  // console.log(ModalInpts);

  const onShowlModal = (event) => {
    if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: null });
    else if (event ? event.kindOf === "showGallery" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: event.name });
  };
  // ============================= remove
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowlModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted.web === images.web ? setImageAccepted({ web: null, phone: null }) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "flag", value: imageAccepted });
  };
  const renderModalInputs = (
    <div className={"width80"}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.kindOf === "showGallery" && (
          <Gallery
            // clickedParent={(value) => inputChangedHandler({ name: ModalInpts.name, value })}
            parentTrue
            acceptedCardInfo={{ handelAcceptedImage: acceptedImage, acceptedCard: imageAccepted }}
          />
        )}
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />}
        <div className="btns-container">
          <button disabled={!imageAccepted.web} className="btns btns-primary" onClick={(e) => acceptedImageFinal(e)}>
            تایید{" "}
          </button>
          <button className="btns btns-warning" onClick={onHideModal}>
            بستن{" "}
          </button>
        </div>
      </ModalBox>
    </div>
  );

  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    // const initialStateTermsOfUse = updateObject(states.addCountry.Form["termsOfUse"], { value: [] });
    // const initialStateFeatures = updateObject(states.addCountry.Form["features"], { value: [] });
    // const updatedForm = updateObject(states.addCountry.Form, { ["TermsOfUse"]: initialStateTermsOfUse, ["features"]: initialStateFeatures });
    if (await post.country(formData)) setData({ ...states.addCountry });
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "flag" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputCountry removeHandel={removeHandel} _onSubmited={_onSubmited} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmited} showlModal={onShowlModal} />;
  return (
    <div className="countainer-main centerAll ">
      {renderModalInputs}
      <div className="form-countainer">
        <div className="form-subtitle">{"افزودن کشور جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCountry;
