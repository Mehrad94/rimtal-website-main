import globalChange from "./globalChange";
import arrayOnchange from "./arrayOnchange";
const onChanges = { globalChange, arrayOnchange };
export default onChanges;
