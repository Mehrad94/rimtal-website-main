import table from "./table";
import states from "./states";
import galleryConstants from "./galleryConstants";
import card from "./card";

const consts = { table, states, galleryConstants, card };
export default consts;
