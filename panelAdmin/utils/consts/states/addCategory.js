const owner = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default owner;
