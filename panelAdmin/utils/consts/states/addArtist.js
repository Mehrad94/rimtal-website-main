const addArtist = {
  Form: {
    nameFa: {
      label: "نام فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام فارسی",
      },
      value: "",
      validation: {
        required: false,
        isFa: false,
      },
      valid: true,
      touched: false,
    },
    nameEn: {
      label: "نام انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " نام انگلیسی",
      },
      value: "",
      validation: {
        required: false,
        isEn: true,
      },
      valid: true,
      touched: false,
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی",
      },
      value: "",
      validation: {
        required: false,
        isFa: false,
      },
      valid: true,
      touched: false,
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی",
      },
      value: "",
      validation: {
        required: false,
        isEn: true,
      },
      valid: true,
      touched: false,
    },
    defaultGenre: {
      label: "ژانر اصلی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر اصلی",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    genre: {
      label: "ژانر های دیگر :",
      elementType: "inputDropDownSearchArray",
      elementConfig: {
        type: "text",
        placeholder: "ژانر های دیگر",
      },
      value: [],
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    birthPlaceCountry: {
      label: "متولد در کشور  :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "متولد در کشور ",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    birthPlaceCity: {
      label: "متولد در شهر  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "متولد در شهر ",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    birthday: {
      label: "تاریخ تولد :",

      elementType: "dateInput",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید ",
      },
      value: { month: "", day: "", year: "" },
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },

    instruments: {
      label: "ساز ها :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ساز ها",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    website: {
      label: "وب سایت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "وب سایت",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    instagram: {
      label: "اینستاگرام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اینستاگرام",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    youTube: {
      label: "یوتیوب :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "یوتیوب",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    twitter: {
      label: "توییتر :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توییتر",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    avatar: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addArtist;
