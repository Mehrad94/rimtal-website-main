import React from "react";

const artist = (data) => {
  console.log({ data });

  const thead = [
    "#",
    "نام فارسی",
    "نام انگلیسی",
    "توضیحات فارسی ",
    "توضیحات انگلیسی",
    "دنبال کنندگان",
    "آلبوم",
    "تک آهنگ",
    " لایک",
    " شنوندگان",
    " ساز",
    " بازدید",
    "موزیک ویدیو",
  ];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let nameFa = data[index].nameFa ? data[index].nameFa : NotEntered;
    let nameEn = data[index].nameEn ? data[index].nameEn : NotEntered;
    let descriptionFa = data[index].descriptionFa ? data[index].descriptionFa : NotEntered;
    let descriptionEn = data[index].descriptionEn ? data[index].descriptionEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albums && data[index].albums.length ? data[index].albums.length : "0";
    let singlesCount = data[index].singles && data[index].singles.length ? data[index].singles.length : "0";
    let musicVideoCounts =
      data[index].musicVideos && data[index].musicVideos.length ? data[index].musicVideos.length : "0";
    let instrumentCounts =
      data[index].instruments && data[index].instruments.length ? data[index].instruments.length : "0";
    let likeCount = data[index].likeCount ? data[index].likeCount : "0";
    let listenCount = data[index].listenCount ? data[index].listenCount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [
        nameFa,
        nameEn,
        descriptionFa,
        descriptionEn,
        followersCount,
        albumsCount,
        singlesCount,
        likeCount,
        listenCount,
        instrumentCounts,
        viewCount,
        musicVideoCounts,
      ],
      style: {},
    });
  }
  return { thead, tbody };
};

export default artist;
