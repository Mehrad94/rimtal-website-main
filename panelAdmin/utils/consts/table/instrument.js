const instrument = (data) => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({ data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts], style: {} });
  }
  return { thead, tbody };
};

export default instrument;
