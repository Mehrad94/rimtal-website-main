import React from "react";

const ShowOwner = (scenario) => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "محدوده  ", " شماره همراه", "موجودی", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < scenario.length; index++) {
    let NotEntered = "وارد نشده";
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    let thumbnail = scenario[index].thumbnail ? scenario[index].thumbnail : NotEntered;
    let title = scenario[index].title ? scenario[index].title : NotEntered;
    let rating = scenario[index].rating ? scenario[index].rating : NotEntered;
    let district = scenario[index].district ? scenario[index].district : NotEntered;
    let phoneNumber = scenario[index].phoneNumber ? scenario[index].phoneNumber : NotEntered;
    let balance = scenario[index].balance ? scenario[index].balance : "0";
    let isActive = scenario[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, { option: { star: true, value: rating } }, district, phoneNumber, balance, isActive, { option: { edit: true } }],
      style: {},
    });
  }
  return [thead, tbody];
};

export default ShowOwner;
