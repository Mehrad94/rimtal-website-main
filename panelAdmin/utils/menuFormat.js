import panelAdmin from "..";
import values from "../values/index";

const menuFormat = [
  {
    title: "عمومی",
    menus: [
      {
        route: values.routes.GS_ADMIN_DASHBOARD,
        menuTitle: values.strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: values.strings.ARTIST,
        menuIconImg: false,
        menuIconClass: "fas fa-user-music",
        subMenu: [
          {
            title: values.strings.SEE_ARTIST,
            route: values.routes.GS_ADMIN_ARTIST,
          },
          {
            title: values.strings.ADD_ARTIST,
            route: values.routes.GS_ADMIN_ADD_ARTIST,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.ALBUM,
        menuIconImg: false,
        menuIconClass: "fas fa-album",
        subMenu: [
          {
            title: values.strings.SEE_ALBUM,
            route: values.routes.GS_ADMIN_ALBUM,
          },
          {
            title: values.strings.ADD_ALBUM,
            route: values.routes.GS_ADMIN_ADD_ALBUM,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.GENRES,
        menuIconImg: false,
        menuIconClass: "fas fa-album",
        subMenu: [
          {
            title: values.strings.SEE_GENRES,
            route: values.routes.GS_ADMIN_GENRE,
          },
          {
            title: values.strings.ADD_GENRE,
            route: values.routes.GS_ADMIN_ADD_GENRE,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.COUNTRY,
        menuIconImg: false,
        menuIconClass: "fas fa-flag",
        subMenu: [
          {
            title: values.strings.SEE_COUNTRY,
            route: values.routes.GS_ADMIN_COUNTRY,
          },
          {
            title: values.strings.ADD_COUNTRY,
            route: values.routes.GS_ADMIN_ADD_COUNTRY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.INSTRUMENT,
        menuIconImg: false,
        menuIconClass: "fas fa-saxophone",
        subMenu: [
          {
            title: values.strings.SEE_INSTRUMENT,
            route: values.routes.GS_ADMIN_INSTRUMENT,
          },
          {
            title: values.strings.ADD_INSTRUMENT,
            route: values.routes.GS_ADMIN_ADD_INSTRUMENT,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.GALLERY,
        menuIconImg: false,
        menuIconClass: "fad fa-images",
        subMenu: [
          {
            title: values.strings.SEE_GALLERY,
            route: values.routes.GS_ADMIN_GALLERY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.SONG,
        menuIconImg: false,
        menuIconClass: "fad fa-comment-music",
        subMenu: [
          {
            title: values.strings.SEE_SONG,
            route: values.routes.GS_ADMIN_SONG,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.MOOD,
        menuIconImg: false,
        menuIconClass: "fas fa-album",
        subMenu: [
          {
            title: values.strings.SEE_MOOD,
            route: values.routes.GS_ADMIN_MOOD,
          },
          {
            title: values.strings.ADD_MOOD,
            route: values.routes.GS_ADMIN_ADD_MOOD,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.HASHTAG,
        menuIconImg: false,
        menuIconClass: "fas fa-album",
        subMenu: [
          {
            title: values.strings.SEE_HASHTAG,
            route: values.routes.GS_ADMIN_HASHTAG,
          },
          {
            title: values.strings.ADD_HASHTAG,
            route: values.routes.GS_ADMIN_ADD_HASHTAG,
          },
        ],
      },
    ],
  },
];

export default menuFormat;
