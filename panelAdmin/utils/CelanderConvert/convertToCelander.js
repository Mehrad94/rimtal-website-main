const convertToCelander = (value) => {
  let valDate,
    day,
    month,
    year,
    selectedDay = null;
  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = { day, month, year };
  }
  return selectedDay;
};
export default convertToCelander;
