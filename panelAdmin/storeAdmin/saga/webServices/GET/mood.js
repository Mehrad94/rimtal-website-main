import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* moodData({ page }) {
  yield put(actions.reduxActions.startGetMood());

  console.log({ mojtaba3: page });
  try {
    const res = yield panelAdmin.api.get.mood({ page });
    console.log({ resArtistData: res });
    yield put(actions.reduxActions.setMoodData(res.data));
  } catch (err) {
    console.log({ errArtistData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* moodSearchData({ page, title }) {
  yield put(actions.reduxActions.startSearchMood());

  //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });

  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });

  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
