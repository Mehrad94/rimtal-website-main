import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* genreData({ page }) {
  yield put(actions.reduxActions.startGetGenre());

  try {
    const res = yield panelAdmin.api.get.genres({ page });
    console.log({ resGenreData: res });
    yield put(actions.reduxActions.setGenreData(res.data));
  } catch (err) {
    console.log({ errGenreData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* genreSearchData({ page, title }) {
  yield put(actions.reduxActions.startSearchGenre());

  //   try {
  //     const res = yield panelAdmin.api.get.GenreSearch({ page, title });
  //     console.log({ resGenreSearchData: res });

  //     yield put(actions.reduxActions.setSearchGenreData(res.data));
  //   } catch (err) {
  //     console.log({ errGenreSearchData: err });

  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
