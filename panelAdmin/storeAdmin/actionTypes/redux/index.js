const atRedux = {
  //======================================================== Redux
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  // ======================================================== NAVBAR
  SET_PAGE_NAME: "SET_PAGE_NAME",

  // ======================================================== UPLOAD
  SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // ======================================================== GALLERY
  SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // ======================================================== ARTIST
  SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // ======================= SEARCH ARTIST
  SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // ======================================================== END ARTIST
  // ======================================================== INSTRUMENT
  SET_INSTRUMENT_DATA: "SET_GALLERY_DATA_REDUX",
  START_INSTRUMENT_DATA: "START_INSTRUMENT_DATA_REDUX",
  // ======================= SEARCH INSTRUMENT
  SET_SEARCH_INSTRUMENT_DATA: "SET_SEARCH_INSTRUMENT_DATA_REDUX",
  START_SEARCH_INSTRUMENT_DATA: "START_SEARCH_INSTRUMENT_DATA_REDUX",
  // ======================================================== END INSTRUMENT

  // ======================================================== COUNTRY
  SET_COUNTRY_DATA: "SET_GALLERY_DATA_REDUX",
  START_COUNTRY_DATA: "START_COUNTRY_DATA_REDUX",
  // ======================= SEARCH COUNTRY
  SET_SEARCH_COUNTRY_DATA: "SET_SEARCH_COUNTRY_DATA_REDUX",
  START_SEARCH_COUNTRY_DATA: "START_SEARCH_COUNTRY_DATA_REDUX",
  // ======================================================== END COUNTRY
  // ======================================================== MOOD
  SET_MOOD_DATA: "SET_GALLERY_DATA_REDUX",
  START_MOOD_DATA: "START_MOOD_DATA_REDUX",
  // ======================= SEARCH MOOD
  SET_SEARCH_MOOD_DATA: "SET_SEARCH_MOOD_DATA_REDUX",
  START_SEARCH_MOOD_DATA: "START_SEARCH_MOOD_DATA_REDUX",
  // ======================================================== END MOOD
  // ======================================================== HASHTAG
  SET_HASHTAG_DATA: "SET_GALLERY_DATA_REDUX",
  START_HASHTAG_DATA: "START_HASHTAG_DATA_REDUX",
  // ======================= SEARCH HASHTAG
  SET_SEARCH_HASHTAG_DATA: "SET_SEARCH_HASHTAG_DATA_REDUX",
  START_SEARCH_HASHTAG_DATA: "START_SEARCH_HASHTAG_DATA_REDUX",
  // ======================================================== END HASHTAG
  // ======================================================== GENRE
  SET_GENRE_DATA: "SET_GALLERY_DATA_REDUX",
  START_GENRE_DATA: "START_GENRE_DATA_REDUX",
  // ======================= SEARCH GENRE
  SET_SEARCH_GENRE_DATA: "SET_SEARCH_GENRE_DATA_REDUX",
  START_SEARCH_GENRE_DATA: "START_SEARCH_GENRE_DATA_REDUX",
  // ======================================================== END GENRE
};

export default atRedux;
