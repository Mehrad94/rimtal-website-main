import panelAdmin from "../..";

export const galleryInitialState = {
  galleryData: [],
};

export const setGalleryData = (state, action) => {
  return { ...state, galleryData: action.data };
};

function galleryReducer(state = galleryInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_GALLERY_DATA:
      return setGalleryData(state, action);
    default:
      return state;
  }
}

export default galleryReducer;
