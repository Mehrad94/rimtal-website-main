import panelAdmin from "../..";

export const artistInitialState = {
  artistData: null,
  searchArtistData: null,
  loading: false,
  searchLoading: false,
};

function artistReducer(state = artistInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ARTIST_DATA:
      return { ...state, ...{ artistData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_ARTIST_DATA:
      return { ...state, ...{ searchArtistData: action.data, searchLoading: false } };
    case atRedux.START_ARTIST_DATA:
      console.log("START_ARTIST_DATA");

      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_ARTIST_DATA:
      console.log("START_SEARCH_ARTIST_DATA");

      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default artistReducer;
