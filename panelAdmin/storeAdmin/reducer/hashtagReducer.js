import panelAdmin from "../..";

export const hashtagInitialState = {
  hashtagData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false,
};

function hashtagReducer(state = hashtagInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_HASHTAG_DATA:
      return { ...state, ...{ hashtagData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_HASHTAG_DATA:
      return { ...state, ...{ searchMoodData: action.data, searchLoading: false } };
    case atRedux.START_HASHTAG_DATA:
      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_HASHTAG_DATA:
      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default hashtagReducer;
