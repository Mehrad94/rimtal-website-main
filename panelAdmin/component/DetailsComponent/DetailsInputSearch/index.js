import React, { useState } from "react";
import InputDropDownSearch from "../../UI/Inputs/InputDropDownSearch";
const DetailsInputSearch = ({ Info, searchInfo, fieldName, label, sendNewVal, toastify }) => {
  const [state, setState] = useState({
    change: false,
    DataValue: Info.value,
    DataAccepted: Info.title
  });

  const _handelEdit = () => {
    setState(prev => ({
      ...prev,
      change: !state.change,
      Info: Info
    }));
  };
  console.log({ state });

  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: { _id: state.DataValue } };
    if (state.Info) sendNewVal(data);
    else toastify("تغییراتی مشاهده نشد", "error");
    _handelEdit();
  };

  const _handelAccepted = value => {
    let index = searchInfo.findIndex(d => d.value === value);
    let accept = searchInfo[index];

    setState(prev => ({ ...prev, DataAccepted: accept.title, DataValue: accept.value }));
  };

  return (
    <div className="card-details-row">
      <div className="about-title">
        <span>{label} :</span> {state.change ? "" : <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>}
        {state.change && (
          <div className="btns-container">
            <a className="btns btns-success" onClick={_handelSendChanged}>
              {" "}
              ثبت{" "}
            </a>
            <a className="btns btns-warning" onClick={_handelEdit}>
              {" "}
              لغو{" "}
            </a>
          </div>
        )}
      </div>
      {state.change ? <InputDropDownSearch accepted={_handelAccepted} dropDownData={searchInfo} value={Info.value} /> : <p>{Info.title}</p>}
    </div>
  );
};

export default DetailsInputSearch;
