import React, { Fragment } from "react";
import { Pagination } from "react-bootstrap";
const Pageination = (props) => {
  const { limited, pages, activePage, onClick } = props;
  let sort = [];

  Number(activePage);
  console.log({ limited, pages, activePage });
  let viewNext = +limited + +activePage;
  let viewPrev = activePage - limited;
  //   console.log({ viewNext, viewPrev });
  for (let index = 1; index <= pages; index++) {
    if (pages >= 9) {
      if (viewPrev <= index && viewNext >= index) sort.push(index);
    } else if (pages <= 9) sort.push(index);
  }

  let pageination_Sort = (
    <div className="pageination-wrapper">
      {" "}
      <Pagination>
        <Pagination.First
          disabled={Number(activePage) < Number(limited)}
          onClick={() => onClick(1)}
        />
        <Pagination.Prev
          disabled={Number(activePage) === Number(1)}
          onClick={() => onClick(activePage - 1)}
        />
        {Number(activePage) > 1 + Number(limited) && pages >= 9 && (
          <Fragment>
            <Pagination.Item onClick={() => onClick(1)}>{1}</Pagination.Item>
            <Pagination.Ellipsis />
          </Fragment>
        )}
        {sort.map((number, index) => {
          if (number === 1 && activePage > +limited + +1 && pages >= 9) return;
          else if (
            number === pages &&
            activePage < pages - limited &&
            pages >= 9
          )
            return;
          else
            return (
              <Pagination.Item
                onClick={() =>
                  onClick(number === Number(activePage) ? false : number)
                }
                key={index + "m"}
                active={number === Number(activePage)}
              >
                {number}
              </Pagination.Item>
            );
        })}

        {activePage < pages - limited && pages >= 9 && (
          <Fragment>
            <Pagination.Ellipsis />

            <Pagination.Item onClick={() => onClick(pages)}>
              {pages}
            </Pagination.Item>
          </Fragment>
        )}
        <Pagination.Next
          disabled={Number(activePage) === Number(pages)}
          onClick={() => onClick(+activePage + +1)}
        />
        <Pagination.Last
          disabled={Number(activePage) > Number(pages - limited)}
          onClick={() => onClick(Number(pages))}
        />
      </Pagination>
    </div>
  );

  return pageination_Sort;
};

export default Pageination;
