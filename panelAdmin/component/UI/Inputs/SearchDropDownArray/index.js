import React, { useState, useEffect, Fragment } from "react";
import AcceptedChild from "./AcceptedChild";
const SearchDropDownArray = (props) => {
  const { removeHandel, onKeyDown, dropDownData, onChange, accepted, disabled, checkSubmited, className, elementConfig, value, staticTitle } = props;
  const [search, setSearch] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [acceptedSave, setAcceptedSave] = useState([]);
  const [typing, setTping] = useState(false);
  const [lock, setLock] = useState(false);
  const [state, setState] = useState({
    removeIndex: null,
    prevValueLength: null,
  });
  let propsVal, index;

  useEffect(() => {
    if (value.length !== state.prevValueLength) {
      console.log(state.prevValueLength, value.length);

      let newState = acceptedSave;
      newState.splice(state.removeIndex, 1);
      setAcceptedSave(newState);
    }
  }, [value.length]);
  // index = dropDownData.findIndex((d) => {
  //   console.log({ dropDownData });

  //   console.log({ valiiiiiiiid: d.value.includes(value) });

  //   return d.value.includes(value);
  // });

  // if (index >= 0) propsVal = dropDownData[index].title;
  // console.log({ indexArrrraaaay: index, propsVal });
  useEffect(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  useEffect(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave([]);
    setTping(false);
  }, [checkSubmited]);
  const handleBlur = (e) => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0)
      setTimeout(() => {
        setSearch([]);
      }, 300);
  };
  const handelOnChange = (e) => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = (index) => {
    let newState = acceptedSave;
    newState.push({ title: search[index].title, value: search[index].value });
    setAcceptedSave(newState);
    setSearchTitle("");
    accepted(search[index].value);
    setLock(true);
    setState({ ...state, prevValueLength: acceptedSave.length });
  };
  console.log({ acceptedSave });
  const _removeHandel = (data) => {
    index = acceptedSave.findIndex((d) => {
      return d.value.includes(data);
    });
    removeHandel(data);
    if (index >= 0) {
      setState({ ...state, removeIndex: index });
    }
  };
  const searchContainer = (
    <div className="text-search-dropDown">
      <div>
        <input onKeyDown={onKeyDown} disabled={disabled} value={searchTitle ? searchTitle : typing ? "" : staticTitle} onBlur={handleBlur} onChange={handelOnChange} className={className} {...elementConfig} />
        {!lock && search && search.length > 0 && (
          <div className="searched-dropDown felxRow">
            {search.map((searched, index) => {
              return (
                <div key={index} onClick={() => clickedElement(index)} className="serached-item">
                  {searched.image ? (
                    <div className="searched-image">
                      {" "}
                      <img src={searched.image} alt="search title" />
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="searched-text">
                    <span className="searched-title">{searched.title}</span>
                    <span className="searched-description">{searched.description}</span>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div>
        <AcceptedChild data={acceptedSave} removeHandel={_removeHandel} />
      </div>
    </div>
  );
  return searchContainer;
};

export default SearchDropDownArray;
