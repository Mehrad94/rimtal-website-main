import React, { useState, useRef, useEffect } from "react";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import DatePicker, { Calendar } from "react-modern-calendar-datepicker";
import panelAdmin from "../../../..";
// import CelanderConvert from "../../../../util/CelanderConvert";
const DateCelander = (props) => {
  const { onKeyDown, disabled, className, elementConfig, value, accepted, checkSubmited } = props;

  const [selectedDay, setSelectedDay] = useState(null);
  const [ShowCelender, setShowCelender] = useState(false);
  const [totalVal, setTotalVal] = useState();
  const [kindOf, setKindOf] = useState("fa");
  const wrapperRef = useRef(null);
  console.log({ selectedDay, ShowCelender, totalVal });
  let converToAd = "تبدیل به میلادی";
  let converTojalili = "تبدیل به شمسی";
  // useEffect(() => {
  //   setSelectedDay(CelanderConvert.convertToDate(value));
  // }, [value]);
  console.log(kindOf);

  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) ShowCelendar();
  };
  const ShowCelendar = () => setShowCelender(!ShowCelender);

  useEffect(() => {
    setSelectedDay();
    setShowCelender();
    setTotalVal();
  }, [checkSubmited]);
  useEffect(() => {
    if (ShowCelender) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });
  const handelChangeKindOf = () => {
    let value;
    if (kindOf === "fa") value = "en";
    else value = "fa";
    setKindOf(value);
  };
  const changeDate = (event) => {
    console.log({ event });

    setSelectedDay(event);
    setTotalVal(panelAdmin.utils.CelanderConvert.convertToDate(event));
    accepted(panelAdmin.utils.CelanderConvert.convertToDate(event));
    setShowCelender();
  };
  const calenderConst = <Calendar value={selectedDay ? selectedDay : CelanderConvert.convertToCelander(value ? value : null)} onChange={changeDate} shouldHighlightWeekends locale={kindOf} />;

  return (
    <div className="relative">
      <div className="box-elements">
        <div onClick={handelChangeKindOf} className="convert-date-title">
          {kindOf === "en" ? converTojalili : converToAd}
        </div>
        <input value={totalVal ? totalVal : value} disabled {...elementConfig} className={className} />
        <span onClick={ShowCelendar}>
          <i className=" icon-calendar" />
        </span>
        {ShowCelender ? (
          <div className="modalClendar" ref={wrapperRef}>
            {calenderConst}
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default DateCelander;
