import React, { useState, useEffect, useMemo } from "react";
import BackgrandCover from "../../BackgrandCover";

const ModalBox = (props) => {
  const { showModal, onHideModal } = props;

  const [modalHide, setModalHide] = useState(true);

  useEffect(() => {
    if (showModal) {
      setModalHide(false);
    }
  }, [showModal]);

  const endAnimation = () => {
    if (!showModal) {
      onHideModal();
      setModalHide(true);
    }
  };

  return (
    <div
      className={"modal_container-box"}
      style={{ display: modalHide ? "none" : "" }}
    >
      <div
        onAnimationEnd={endAnimation}
        id="subjectModal"
        className={showModal ? "fadeIn" : "fadeOut"}
      >
        <div className="subjectModal-box">{props.children}</div>
      </div>
      <BackgrandCover onClick={onHideModal} fadeIn={!modalHide} />
    </div>
  );
};

export default ModalBox;
