import React from "react";
const ModalTrueFalse = (props) => {
  return (
    <div className="modal-box">
      <div className="m-b-question-title">
        <span>{props.modalHeadline}</span>
        <span>{props.modalQuestion}</span>
      </div>
      <div className="btns-container">
        <div onClick={() => props.modalAccept(true)} className="submited-box">
          <a className="btns btns-primary" type="submit">
            {props.modalAcceptTitle}{" "}
          </a>
        </div>
        <div onClick={() => props.modalAccept(false)} className="submited-box">
          <a className="btns btns-warning" type="submit">
            {" "}
            {props.modalCanselTitle}
          </a>
        </div>
      </div>
    </div>
  );
};

export default ModalTrueFalse;
