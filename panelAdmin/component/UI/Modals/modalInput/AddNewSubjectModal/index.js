import React from "react";
import "./index.css";
import InputFile from "../../../../components/InputsComponent/file";
import InputText from "../../../../components/InputsComponent/text";

const AddNewSubjectModal = props => {
  const { type, returns, onCancel, fileValue, onChange, value, acceptedTitle, showModal, endAnimation, subTitle } = props;
  return (
    <div onAnimationEnd={endAnimation} id="subjectModal" className={showModal ? "fadeIn" : "fadeOut"}>
      <div className="subjectModal-box">
        <div className="subjectModal-AddInformation">
          <label>{subTitle}</label>
          {type === "file" ? (
            <InputFile inputLabel={"افزودن فایل"} inputval={fileValue} onFileSelected={onChange} />
          ) : (
            <InputText inputType={"text"} onTextChange={onChange} inputval={value} />
          )}
        </div>
        <div className="btn-submited-container">
          <div className="submitedBtn">
            <button onClick={returns}>{acceptedTitle} </button>
          </div>
          <div className="submitedBtn closed">
            <button onClick={onCancel}>انصراف</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddNewSubjectModal;
