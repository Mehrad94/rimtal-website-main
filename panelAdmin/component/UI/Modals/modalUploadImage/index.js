import React from "react";
import { Modal, Button } from "react-bootstrap";

const ModalUploadImage = ({ status, setStatus, onChange, title, onSubmit }) => {
  return (
    <React.Fragment>
      <Modal show={status} onHide={() => setStatus(false)} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className="input-upload-name">
              <label>Name:</label>
              <input />
            </div>
            <div className="input-upload-image my-3">
              <label>Image:</label>
              <input type="file" onChange={onChange} />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setStatus(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={onSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default ModalUploadImage;
