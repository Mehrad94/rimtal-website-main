import * as React from "react";
import CardRow from "./CardRow";
import LazyImage from "../../../../components/LazyImage";

const CardElement = React.memo((props) => {
  const { data, index, onClick, submitedTitle, optionClick, options, acceptedCardInfo } = props;
  // console.log({ data });

  return (
    <div
      style={{ animationDelay: index * 150 + "ms" }}
      key={index ? index : ""}
      className={`show-Card-Information-row px-1  col-xl-2 col-lg-2 col-6 col-sm-6 col-md-6`}
      onClick={acceptedCardInfo ? () => acceptedCardInfo.handelAcceptedImage({ index, data }) : null}
    >
      <div
        //  style={{ boxShadow: data.isActive ? "" : "0 0 6px 3px #ff00008a" }}
        className={`card-info transition0-2 ${data.isAccept}`}
      >
        <div className="s-c--card-images transition0-2">
          <div className="options-card transition0-2">
            {options ? (
              <Fragment>
                {" "}
                {options.remove && (
                  <span className={"options-card-cancel"} onClick={() => optionClick({ _id: data._id, mission: "remove" })}>
                    <i className={" icon-cancel"} />
                  </span>
                )}
                {options.edit && (
                  <span className={"options-card-edit"} onClick={() => optionClick({ _id: data._id, mission: "edit" })}>
                    <i className={" icon-pencil-2"} />
                  </span>
                )}{" "}
                {options.block && (
                  <span className={"options-card-block"} onClick={() => optionClick({ _id: data._id, mission: "block", value: !data.isActive })}>
                    <i className={"icon-lock-1"} />
                  </span>
                )}
              </Fragment>
            ) : (
              ""
            )}
          </div>
          {data.image ? (
            <picture>
              <source media="(max-width: 375px)" srcSet={data.image && data.image.value.phone} />

              {/* <img id="myImage" className="noSelect noEvent" src={images.web} alt={titleTop} /> */}
              <LazyImage src={data.image && data.image.value.web} defaultImage={false} alt={"cardImage"} />
            </picture>
          ) : (
            ""
          )}
          {/* <img src={data.image && data.image.value} alt="cardImage" /> */}
        </div>

        <div className="s-c-card-body">
          <div className="s-c-body-wrapper">
            <div className="s-c-card-title">
              {data.body &&
                data.body.length > 0 &&
                data.body.map((info, index) => {
                  return (
                    <div key={index + "mmj"}>
                      <div>{info.right && info.right.map((right) => CardRow(right))}</div>
                      <div>{info.left && info.left.map((left) => CardRow(left))}</div>
                    </div>
                  );
                })}
            </div>
          </div>
          {submitedTitle ? (
            <div className="btns-container">
              <button onClick={() => onClick(index)} className="btns btns-primary">
                {submitedTitle}
              </button>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
});
export default CardElement;
