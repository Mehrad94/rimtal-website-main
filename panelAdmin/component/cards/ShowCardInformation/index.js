import React from "react";
import CardElement from "../CardElement";
import Scrollbar from "react-scrollbars-custom";
import PerfectScrollbar from "react-perfect-scrollbar";

const ShowCardInformation = ({ data, onClick, submitedTitle, optionClick, options, acceptedCardInfo }) => {
  const showDataAll = data.map((information, index) => {
    // //console.log({ information });
    return <CardElement key={index + "mmm"} data={information} options={options} optionClick={optionClick} index={index} onClick={onClick} submitedTitle={submitedTitle} acceptedCardInfo={acceptedCardInfo} />;
  });
  const ShowData = <CardElement data={data} onClick={onClick} options={options} optionClick={optionClick} submitedTitle={submitedTitle} acceptedCardInfo={acceptedCardInfo} />;

  return <div className="show-card-elements row m-0">{data.length > 0 ? showDataAll : ShowData}</div>;
};

export default ShowCardInformation;
