import React from "react";
import Link from "next/link";
import SubMenu from "../SubMenu";

const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation,
}) => {
  const location = windowLocation;

  const activedSideTitle = (name) => {
    let newName = name;
    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }
    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [
      length ? "icon-right-open " : "",
      // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
      showLi === title ? "arrowRotate" : "unsetRotate",
    ].join(" ");
    return classes;
  };
  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return (
      <li key={"menus-" + index} className="side-iteme">
        <Link href={!menu.subMenu.length > 0 ? menu.route : "#"}>
          <a
            onClick={
              menu.route
                ? _handelStateNull
                : () => activedSideTitle(menu.subMenu && menu.menuTitle)
            }
            id={
              location.includes(menu.route)
                ? "activedSide"
                : selectedMenuTitle === menu.menuTitle
                ? showLi === menu.menuTitle
                  ? ""
                  : "activedSide"
                : ""
            }
            className={`side-link ${classNameForMenu(
              menu.subMenu.length,
              menu.menuTitle
            )}`}
          >
            {menu.menuIconImg ? (
              <img src={menu.menuIconImg} alt="icon menu" />
            ) : (
              <i className={menu.menuIconClass}></i>
            )}
            <span>{menu.menuTitle}</span>
            {menu.subMenu.length ? (
              <div className="menu-arrow-icon">
                <i className="fas fa-angle-left"></i>
              </div>
            ) : (
              ""
            )}
          </a>
        </Link>
        <SubMenu
          windowLocation={windowLocation}
          menu={menu}
          setMenuTitle={setMenuTitle}
          showLi={showLi}
          selectedMenu={selectedMenu}
        />
      </li>
    );
  });
};

export default Menu;
