import React from "react";
import "./index.scss";
const VideoPlayer = ({ src, type, width }) => {
  //console.log(src, type);

  return (
    <div className="show-video-src-container">
      <video controls>
        <source src={src} type={type ? type : "video/mp4"} />
        {/* <source src="mov_bbb.ogg" type="video/ogg" /> */}
      </video>
    </div>
  );
};

export default VideoPlayer;
