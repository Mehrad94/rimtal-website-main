import SliderCard from "../../components/cards/SliderCard";
import MusicCardDouble from "../../components/Cards/MusicCardDouble";
import convertData from "../../utils/convert";
import strings from "../values/strings";
import MusicCard from "../../components/Cards/MusicCard";
const homeCore = (data) => {
  let returnData = [
    { Row: SliderCard, data: convertData.sliderCard(data.sliders) },
    { Row: MusicCardDouble, data: convertData.albumCard(data.albums), headerDetails: { headline: strings.ALBUMS, locationDetails: { title: strings.VIEW_ALL, location: { href: "#", as: "#" } } } },
    { Row: MusicCardDouble, data: convertData.singleCard(data.singles), headerDetails: { headline: strings.SINGLES, locationDetails: { title: strings.VIEW_ALL, location: { href: "#", as: "#" } } } },
    { Row: MusicCard, data: convertData.comingSoonCard(data.comingSoon), headerDetails: { headline: strings.COMING_SOON, locationDetails: { title: strings.VIEW_ALL, location: { href: "#", as: "#" } } } },
  ];
  return returnData;
};
export default homeCore;
