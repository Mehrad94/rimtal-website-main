import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import themeColorReducer from "./themeColorReducer";
import panelAdmin from "../../panelAdmin";
import website from "../../website";

const rootReducer = combineReducers({
  error: errorReducer,
  themeColor: themeColorReducer,
  ...panelAdmin.reducer,
  ...website.reducer,
});

export default rootReducer;
