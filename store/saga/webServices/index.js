import GET from "./GET";
import POST from "./POST";

const webServices = {
  GET,
  POST,
};

export default webServices;
