import { takeEvery, put } from "redux-saga/effects";
import atSaga from "../actionTypes/saga";
import actions from "../actions";
import webServices from "./webServices";

// ============================= HOME
export function* watchGetHomeData() {
  yield takeEvery(atSaga.GET_HOME_SCREEN_DATA, webServices.GET.homeData);
}
// ============================= HOME
