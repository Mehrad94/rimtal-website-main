const atRedux = {
  //======================================================== Redux

  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  // // ======================================================== UPLOAD
  // SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // // ======================================================== GALLERY
  // SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  // CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // // ======================================================== ARTIST
  // SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  // START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // // ======================= SEARCH ARTIST
  // SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  // START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // // ======================================================== END ARTIST
  // // ======================================================== NAVBAR
  // SET_PAGE_NAME: "SET_PAGE_NAME",
};

export default atRedux;
