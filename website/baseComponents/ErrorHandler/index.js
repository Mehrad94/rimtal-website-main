import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const ErrorHandler = () => {
  const error = useSelector((state) => {
    // console.log({ErrorHandler: state.error});
    return state.error;
  });
  useEffect(() => {
    switch (error.error) {
      case 1098:
        alert("خطایی در سرور رخ داده");
        break;
      case 1090:
        alert("اتصال به اینترنت را بررسی کنید");
        break;
    }
  }, [error]);
  return <></>;
};

export default ErrorHandler;
