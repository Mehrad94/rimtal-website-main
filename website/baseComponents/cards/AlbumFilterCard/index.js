import React, { useState, Fragment, useRef, useEffect } from "react";

const AlbumFilterCard = ({ filters, onFilterPress }) => {
  //fot setting the title
  console.log({ FOX: filters });

  const [titleNameState, setTitleNameState] = useState(filters.title);

  //for handling title name on change
  const changeHandler = (title) => {
    const filterData = {
      type: filters.title,
      value: title,
    };
    onFilterPress(filterData);
    setTitleNameState(title);
  };

  //for set dropdown to true and false
  const [dropclass, setDropClass] = useState({
    showDropDown: false,
    clickedComponent: false,
  });
  const wrapperRef = useRef(null);

  //for handling dropdown menu on click
  const onclickHandler = () => {
    setDropClass((prev) => ({
      ...prev,
      showDropDown: !dropclass.showDropDown,
      clickedComponent: true,
    }));
  };
  //for handling drop down menu on click on another element
  const outsideClickHandler = (e) => {
    if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
      onclickHandler();
    }
  };

  //for handling the click in window
  useEffect(() => {
    if (dropclass.showDropDown) {
      document.addEventListener("click", outsideClickHandler);
      return () => {
        document.removeEventListener("click", outsideClickHandler);
      };
    }
  });

  return (
    <div className="collapse navbar-collapse" id="collapse-show">
      <li
        className={
          dropclass.showDropDown ? "drp" : dropclass.clickedComponent ? "drp-deactive" : ""
        }
      >
        <div
          onClick={() => {
            onclickHandler();
          }}
          ref={wrapperRef}
          className="title-wrapper"
        >
          <span>{titleNameState}</span>
          <i className="fal fa-chevron-down"></i>
        </div>

        <ul className="drop-down-wrapper">
          {filters.child.map((chil) => {
            return (
              <li
                className={`${chil.subTitle === titleNameState ? "active" : ""}`}
                onClick={() => changeHandler(chil.subTitle)}
              >
                {chil.subTitle}
              </li>
            );
          })}
        </ul>
      </li>
    </div>
  );
};

export default AlbumFilterCard;
