import React, { useState } from "react";
import string from "../../values/strings";
import themeColor from "../../values/theme/themeColor";
import Link from "next/link";
import placeholder from "../../../../public/assets/images/placeholder/albumPlaceholder.png";
// ("../../../../public/assets/images/placeholder/albumPlaceholder.png");
import MusicCardDouble from "../../../components/cards/MusicCardDouble";
import website from "../..";
import AwesomeScroll from "../../../../components/AwesomeScroll";
const modalData = [
  { icon: "far fa-stream", title: "Add To Quque" },
  { icon: "far fa-album", title: "Go To Album" },
  { icon: "fal fa-plus", title: "Add To Playlist", child: [] },
  { icon: "fas fa-share", title: "Share", child: [] },
];
const AlbumContainer = (props) => {
  const { albums } = props;

  const [state, setState] = useState({
    modal: { show: false, data: modalData },
  });
  const toggleDotIconClick = () => {
    // console.log("omad");
    setState((prev) => ({ ...prev, modal: { show: !prev.modal.show } }));
  };
  // console.log({ albums });
  const newData = website.convert.albumCard(albums);
  // console.log({ newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{string.ALBUMS}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{string.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>

      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCardDouble key={"AlbumCard-" + index} data={data} placeholder={placeholder} dotIconClick={toggleDotIconClick} dotModalInfo={state.modal} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default AlbumContainer;
