import website from "../..";

const sliderCard = (data, direction) => {
  let convertData = [];

  // console.log({PAPPAAPA:data});
  for (const index in data) {
    let noEntries = website.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let parentName = data[index].parentName
      ? data[index].parentName
      : noEntries;
    let parentArtist = data[index].parentArtist
      ? data[index].parentArtist
      : noEntries;
    convertData.push({
      titleTop: title,
      titleMiddle: parentName,
      titleBottom: parentArtist,
      images: data[index].images,
      location: { href: "/slider", as: `/slider#` },
    });
  }
  return convertData;
};
export default sliderCard;
