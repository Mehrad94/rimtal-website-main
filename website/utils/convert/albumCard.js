import chunkArray from "../chunkArray";
const albumCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;
    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: { href: "#", as: `#` },
      });
    }
    if (chunkNumber) convertData = chunkArray(convertData, chunkNumber);
    // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    return convertData;
  }
};
export default albumCard;
