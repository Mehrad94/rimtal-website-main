import atSaga from "../../actionTypes/saga";

export function getHomeData() {
  console.log("getHomeData");

  return { type: atSaga.GET_HOME_SCREEN_DATA };
}
