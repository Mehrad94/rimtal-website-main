import atRedux from "../../actionTypes/redux";

export function increment() {
  return { type: atRedux.INCREMENT };
}
// // =================================================== NAVBAR

// =================================================== HOME
export function setHomeData(data) {
  return { type: atRedux.SET_HOME_DATA, data };
}
// =================================================== ERROR
export function setFailure(error) {
  return { type: atRedux.ADD_FAILURE, error };
}
export function filterType(type) {
  return { type: atRedux.SET_FILTER_TYPE, data: type };
}
export function removeFilterType() {
  return { type: atRedux.REMOVE_FILTER_TYPE };
}
