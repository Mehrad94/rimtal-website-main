import React, { Fragment } from "react";
import GenreHeaderContainer from "./GenreHeaderContainer";
import GenreMainContainer from "./GenreMainContent";

const GenreScreen = () => {
  return (
    <Fragment>
      <GenreHeaderContainer />
      <GenreMainContainer />
    </Fragment>
  );
};

export default GenreScreen;
