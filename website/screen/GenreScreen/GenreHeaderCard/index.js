import React from "react";

const GenreHeaderCard = (props) => {
  return (
    <div className="genre-container">
      <ul className="title-wrapper-top">
        <li>
          <h1>Rock</h1>
        </li>
        <li>
          <i class="fas fa-play"></i>
        </li>
        <li>
          <i class="fal fa-heart"></i>
        </li>
        <li>
          <i class="fal fa-bell"></i>
        </li>
      </ul>
      <ul className="categories-wrapper">{props.title}</ul>
    </div>
  );
};

export default GenreHeaderCard;
