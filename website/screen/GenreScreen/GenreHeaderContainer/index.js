import React, { useState } from "react";
import GenreHeaderCard from "../GenreHeaderCard";

const GenreHeaderContainer = () => {
  const titles = [
    { id: 100, title: "overview" },
    { id: 200, title: "new release" },
    { id: 300, title: "top songs" },
    { id: 400, title: "essential album" },
    { id: 500, title: "videos" },
    { id: 600, title: "playlists" },
  ];

  const [selectedGenre, setSelectedGenre] = useState("");

  const activeHandler = (index) => {
    setSelectedGenre(index);
  };
  const renderTitles = () => {
    return titles.map((title) => {
      return (
        <li
          className={selectedGenre === title.id ? "active-class" : ""}
          onClick={() => activeHandler(title.id)}
        >
          {title.title}
        </li>
      );
    });
  };
  return (
    <React.Fragment>
      <GenreHeaderCard title={renderTitles()} />
    </React.Fragment>
  );
};

export default GenreHeaderContainer;
