import React, { useState, useEffect } from "react";
import MusicCard from "../../../../components/cards/cardElements/MusicCard";
import axios from "axios";
import website from "../../../..";
import placeholder from "../../../../../public/assets/images/placeholder/albumPlaceholder.png";
import AwesomeScroll from "../../../../../components/AwesomeScroll";

const EssentialAlbum = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await axios.get("https://rimtal.com/api/v1/albums/1");
    const allData = res.data;
    if (data.length) allData.docs.push(data);
    setData(res.data);
  };
  console.log({ data });

  const editedData = website.utils.convert.comingSoonCard(data?.docs);
  return (
    <div className="essential-wrapper">
      <h1>Essential Album</h1>
      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {editedData &&
            editedData.map((info, index) => {
              return <MusicCard key={"essential-" + index} data={info} placeholder={placeholder} parentClass={" col-lg-2 col-md-3 col-5  px-0"} />;
            })}
        </ul>
      </AwesomeScroll>
    </div>
  );
};

export default EssentialAlbum;
