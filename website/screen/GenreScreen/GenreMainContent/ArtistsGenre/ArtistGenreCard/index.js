import React from "react";

const ArtistGenreCard = ({ artist }) => {
  return (
    <figure className="col-5 col-md-3 col-lg-2 px-0">
      <img src={artist.image} alt="artist" />
      <figcaption>
        <h2>{artist.artistName}</h2>
        <p>{artist.fans}</p>
        <span>Fans</span>
      </figcaption>
    </figure>
  );
};

export default ArtistGenreCard;
