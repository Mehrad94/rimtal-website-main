import React, { Fragment } from "react";
import TopTracksCard from "../TopTracksCard";
import website from "../../../../..";
import AwesomeScroll from "../../../../../../components/AwesomeScroll";

const TopTracksConitainer = () => {
  const datas = [
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
    {
      cover: "https://i.picsum.photos/id/651/50/50.jpg",
      title: "sub",
      artist: "artist name",
    },
  ];

  const newData = website.utils.convert.topTrackCard(datas, 4);
  console.log({ newData });

  const renderDatas = () => (
    <div className="tracks-container">
      <h1>Top tracks</h1>
      <AwesomeScroll scrollBar={true}>
        <div className="tracks-wrapper row">
          {newData.map((data) => (
            <div className="col-lg-7">
              {data.map((info) => {
                return <TopTracksCard data={info} />;
              })}
            </div>
          ))}
        </div>
      </AwesomeScroll>
    </div>
  );
  return <Fragment>{renderDatas()}</Fragment>;
};

export default TopTracksConitainer;
