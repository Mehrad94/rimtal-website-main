import React from "react";
import AlbumContainer from "./AlbumContainer";
import AlbumFilterContainer from "./AlbumFilterContainer";

const AlbumScreen = () => {
  return (
    <React.Fragment>
      <AlbumFilterContainer />
      <AlbumContainer />
    </React.Fragment>
  );
};

export default AlbumScreen;
