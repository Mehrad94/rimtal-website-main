import React, { useEffect } from "react";
import AlbumFilterCard from "../../../baseComponents/cards/AlbumFilterCard";
import { useDispatch } from "react-redux";
import action from "../../../storeWebsite/actions/index";
import { getAlbumsFilter } from "../../../api/Get/getAlbumsFilter";
const AlbumFilterContainer = () => {
  const dispatch = useDispatch();
  const filters = [
    {
      title: "All",
      child: [{ subTitle: "All" }, { subTitle: "1week" }, { subTitle: "1month" }],
    },
    {
      title: "Type",
      child: [
        { subTitle: "AllType" },
        { subTitle: "most popular" },
        { subTitle: "most played" },
        { subTitle: "most liked" },
        { subTitle: "most of rate" },
        { subTitle: "album sales" },
      ],
    },
    {
      title: "All Genre",
      child: [
        { subTitle: "rock" },
        { subTitle: "instrumental" },
        { subTitle: "alternative" },
        { subTitle: "trip hop" },
        { subTitle: "hip hop" },
        { subTitle: "pop" },
        { subTitle: "electronic" },
      ],
    },
    {
      title: "All Instrument",
      child: [
        { subTitle: "piano" },
        { subTitle: "guitar" },
        { subTitle: "guitar electric" },
        { subTitle: "setar" },
      ],
    },
    {
      title: "All hashtag",
      child: [{ subTitle: "music" }, { subTitle: "rock" }, { subTitle: "rap" }],
    },
    {
      title: "All mood",
      child: [{ subTitle: "relax" }, { subTitle: "sleeppy" }, { subTitle: "chill" }],
    },
  ];
  useEffect(() => {
    getfilerData();
  }, []);
  const getfilerData = async () => {
    const res = await getAlbumsFilter();
    console.log({ FOXRES: res });
  };
  const onFilterPress = (filter) => {
    if (filter.type === "All") {
      dispatch(action.reduxActions.removeFilterType());
    } else {
      dispatch(action.reduxActions.filterType(filter));
    }
  };
  const renderItems = () => {
    console.log({ Mehrad: filters });
    return filters.map((filter) => {
      return <AlbumFilterCard onFilterPress={onFilterPress} filters={filter} />;
    });
  };

  return (
    <div className="albums-container">
      <div className="albums-header-wrappper navbar-expand-lg">
        <h1>Albums</h1>

        <ul className="albums-nav flex-column flex-lg-row">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#collapse-show"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i class="far fa-bars"></i>
          </button>
          {renderItems()}
        </ul>
      </div>
    </div>
  );
};

export default AlbumFilterContainer;
