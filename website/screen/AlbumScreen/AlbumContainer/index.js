import React, { useEffect, useState, useRef } from "react";
import MusicCard from "../../../components/cards/cardElements/MusicCard";
import axios from "axios";
import website from "../../..";
import placeholder from "../../../../public/assets/images/placeholder/albumPlaceholder.png";
import InfiniteScroll from "react-infinite-scroller";
import { filteredData } from "../../../api/POST/filterType";
import { useSelector } from "react-redux";
const AlbumContainer = () => {
  const [data, setData] = useState([]);
  const [fData, setFData] = useState(null);
  const [serverData, setServerData] = useState([]);
  const ref = useRef(null);
  const store = useSelector((state) => {
    return state.filter;
  });
  console.log({ store });
  const filterType = {
    type: "time",
    from: "2020-01-01",
    days: 30,
  };
  console.log({ fData });

  useEffect(() => {
    postData();
  }, []);
  const postData = async () => {
    const res = await filteredData(filterType);
    res.data && setFData(res.data);
    res.error && setFData(res.error);
  };
  const getData = async (page = 1) => {
    // console.log(`=========== GET PAGE = ${page} =============`);
    const res = await axios.get(`https://rimtal.com/api/v1/albums/${page}`);
    if (res.data.docs.length > 0) {
      const newArman = data.concat(res.data.docs);
      setData(newArman);
      setServerData(res.data);
    } else {
      console.log("Nothing found");
    }
  };

  const newData = website.utils.convert.albumCard(data);
  const loadMore = (e) => {
    if (e > serverData?.pages) {
      return;
    } else {
      getData(e);
    }
  };

  return (
    <main className="main">
      <div className="data-main-wrapper" ref={ref}>
        <InfiniteScroll
          className="flexing"
          pageStart={0}
          loadMore={loadMore}
          hasMore={serverData?.page ? parseInt(serverData.page) < serverData.pages : true}
        >
          {newData &&
            newData.map((info, index) => {
              return (
                <MusicCard
                  key={"AlbumCard-" + index}
                  data={info}
                  placeholder={placeholder}
                  parentClass={" col-lg-2 col-md-3 col-6 px-0"}
                />
              );
            })}
        </InfiniteScroll>
      </div>
    </main>
  );
};

export default AlbumContainer;
