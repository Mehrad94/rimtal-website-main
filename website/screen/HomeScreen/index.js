import React, { useState, useEffect } from "react";
import * as sagaActions from "../../../store/actions/saga";
import { useSelector, useDispatch } from "react-redux";
import Loading from "../../../utils/Loading";
import Header from "../../components/Header/index/index";
import SliderCardContainer from "../../components/containers/SliderCardContainer";
import SingleContainer from "../../components/containers/SingleContainer";
import AlbumContainer from "../../components/containers/AlbumContainer";
import MoodCardContainer from "../../components/containers/MoodCardContainer";
import PlaylistContainer from "../../components/containers/PlaylistContainer";
import VideoCardContainer from "../../components/containers/VideoCardContainer";
import PlaylistForAutuminContainer from "../../components/containers/PlaylistForAutuminContainer";
import ComingSoonContainer from "../../components/containers/ComingSoonContainer";
import website from "../..";
// import dynamic from "next/dynamic";

// const DesktopContent = dynamic(() => import("./desktop-content"));
// const MobileContent = dynamic(() => import("./mobile-content"));
const HomeScreen = (props) => {
  const [isLoading, setLoading] = useState(true);

  //Get Home Data

  const [direction, setDirection] = useState("ltr");
  // Connect to Store
  const home = useSelector((state) => {
    return state.home.homeData;
  });

  useEffect(() => {
    console.log({ home });
    if (home.sliders) setLoading(false);
  }, [home]);

  const main = () => (
    <main className="main-screen-wrapper padding-top">
      <SliderCardContainer sliders={home.sliders} />
      <MoodCardContainer moods={home.moods} />
      <AlbumContainer albums={home.albums} />
      <SingleContainer singles={home.singles} />
      <PlaylistContainer playlists={home.playlists} />
      <VideoCardContainer musicVideos={home.musicVideos} />
      <PlaylistForAutuminContainer PlaylistForAutumins={home.suggestedPlaylists} />
      <ComingSoonContainer comingSoon={home.comingSoon} />
    </main>
  );

  return isLoading ? "" : main();
};

export default HomeScreen;
