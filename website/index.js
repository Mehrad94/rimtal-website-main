import values from "./values";
import utils from "./utils";
import reducer from "./storeWebsite/reducer";
const website = { values, utils, reducer };
export default website;
