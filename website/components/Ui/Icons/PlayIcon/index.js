import React from "react";
// import "./index.scss";
const PlayIcon = ({ style, className }) => {
  return (
    <div className={`icon-wrapper ${className}`}>
      {/* <svg viewBox="0 0 33 36" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.879 1.053c3.7-2.14 29.61 12.84 29.61 17.12 0 4.28-26.048 19.18-29.61 17.12-3.562-2.059-3.701-32.1 0-34.24z" fill="currentcolor" fillRule="evenodd"></path>
      </svg> */}
      <i className="fas fa-play"></i>
    </div>
  );
};

export default PlayIcon;
