import React from "react";
// import "./index.scss";
const StopIcon = () => {
  return (
    <div className="play-pause-feedback ">
      <svg viewBox="0 0 11 14" xmlns="http://www.w3.org/2000/svg">
        <g fill="currentcolor" fill-rule="evenodd">
          <rect x="7" width="4" height="14" rx="2"></rect>
          <rect width="4" height="14" rx="2"></rect>
        </g>
      </svg>
    </div>
  );
};

export default StopIcon;
