import React, { useState, useEffect, useRef } from "react";

const DotIcon = ({ click, overflowUnset }) => {
  const wrapperRef = useRef(null);
  const modalData = [
    { icon: "far fa-stream", title: "Add To Quque" },
    { icon: "far fa-album", title: "Go To Album" },
    { icon: "fal fa-plus", title: "Add To Playlist", child: [] },
    { icon: "fas fa-share", title: "Share", child: [] },
  ];
  const [modal, setModal] = useState({
    show: false,
    data: modalData,
  });

  const toggleDotIconClick = () => {
    console.log("toggleDotIconClick");
    // console.log({ wrapperRef: wrapperRef.current });
    setModal((prev) => ({ ...prev, show: !prev.show }));
  };
  const handleClickOutside = (event) => {
    if (modal.show)
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        toggleDotIconClick();
      }
    //  else if (wrapperRef.current && wrapperRef.current.contains(event.target)) {
    //   console.log({ handleClickOutside: event, screenX: event.screenX, screenY: event.screenY });
    // }
  };
  const showModal = (event) => {
    console.log({ showModal: event });
  };
  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  });

  const modalElement = (
    <div className={`small-modal-card-container ${modal && modal.show ? "showModalElement" : ""}`}>
      <div className="small-modal-card-wrapper">
        {modal &&
          modal.data.map((data, index) => {
            return (
              <div key={"modal-" + index} className="modal-element-details">
                <i className={data.icon} />
                <div className="title-arrow">
                  <div className="small-modal-card-title">{data.title}</div>
                  {data.child ? (
                    <div className="chevron-arrow-dropDown">
                      <i className="far fa-chevron-right"></i>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
  const moseleave = () => {
    if (modal.show) toggleDotIconClick();
  };
  return (
    <div onMouseLeave={moseleave} ref={wrapperRef} className=" icon-ellipsis relative centerAll pointer" style={{ color: "white" }}>
      <div onClick={toggleDotIconClick} className="">
        <i className="far fa-ellipsis-h"></i>
      </div>
      {/* {modal && modal.show ? modalElement : ""} */}
      {modalElement}
    </div>
  );
};

export default DotIcon;
