import React, { useState } from "react";
const HeartIcon = ({ style }) => {
  const [actived, setActived] = useState(false);
  const _handelClick = () => {
    setActived(!actived);
  };
  return (
    <div onClick={_handelClick} style={style} className="icon-wrapper">
      {actived ? <i style={{ color: "red" }} className="fas fa-heart" /> : <i className="far fa-heart" />}
    </div>
  );
};

export default HeartIcon;
