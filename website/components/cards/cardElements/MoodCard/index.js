import React, { useRef } from "react";
import placeholder from "../../../../../public/assets/images/placeholder/videoplaceholder.png";
import LazyImage from "../../../../../components/LazyImage";

const MoodCard = (props) => {
  const { title, images, alt } = props.data;
  const imageOnload = () => {
    // optionRef.current.style.display = "flex";
  };
  return (
    <li className="card-Mood-container noSelect  col-lg-2 col-md-3 col-5 px-0">
      <div className=" card-Mood-wrapper">
        <div className="imageCard noSelect noEvent">
          <div className="descriptionCard-middle">
            <span className="card-mood-title">{title}</span>
          </div>
          <picture>
            <source media="(max-width: 375px)" srcSet={images.phone} />
            {/* <img id="myImage" className="noSelect noEvent" src={images.web} alt={title} /> */}
            <LazyImage imageOnload={imageOnload} defaultImage={placeholder} src={images.web} alt="placeholder" />
          </picture>
        </div>
      </div>
    </li>
  );
};

export default MoodCard;
