import React, { useRef, useEffect } from "react";
import Link from "next/link";
import PlayIcon from "../../../Ui/Icons/PlayIcon";
import LazyImage from "../../../../../components/LazyImage";
const SliderCard = (props) => {
  const { titleTop, titleMiddle, titleBottom, images } = props.data;

  const optionRef = useRef(null);
  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };
  // console.log({SliderCardProps:props});
  // console.log({ SliderCardProps: props });
  // useEffect(() => {
  //   console.log({ liRef: liRef.current });
  // }, []);
  return (
    <li className="slider-card-container col-lg-5 col-md-7 col-9 px-0">
      <div className="slider-card-wrapper">
        <div className="slider-card-title">
          <h4>{titleTop}</h4>
          <span>{titleMiddle}</span>
          <h3>{titleBottom}</h3>
        </div>
        <div className="slider-card-image">
          <picture>
            <source media="(max-width: 375px)" srcSet={images.phone} />
            <LazyImage
              imageOnload={imageOnload}
              id="myImage"
              className="noSelect noEvent"
              defaultImage={props.placeholder}
              src={images.phone}
              alt={titleTop}
            />
          </picture>
          <div ref={optionRef} className="card-options">
            <a>
              <PlayIcon
                // className={classNameNew}
                style={{ fontSize: "1em", margin: "0.7em" }}
              />
            </a>
          </div>
        </div>
      </div>
    </li>
  );
};

export default SliderCard;
