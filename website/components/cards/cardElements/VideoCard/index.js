import React, { useRef, useEffect } from "react";
import PlayIcon from "../../../Ui/Icons/PlayIcon";
// import "./index.scss";
import Link from "next/link";
import placeholder from "../../../../../public/assets/images/placeholder/videoplaceholder.png";
import LazyImage from "../../../../../components/LazyImage";
const VideoCard = (props) => {
  // console.log({ VideoCardprops: props });

  const { titleTop, titleMiddle, titleBottom, images } = props.data;
  const optionRef = useRef(null);
  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };
  // const imageRef = useRef(null);
  // let style = {
  //   marginRight: "1.5em",
  // };
  // let classNameNew = "";

  // let titleStyle = {
  //   marginLeft: "0",
  // };
  // if (directionWeb === "rtl") {
  //   style = { marginLeft: "1.5em" };
  //   titleStyle = { marginRight: "0" };
  //   classNameNew = "Rotate180";
  // }

  // useEffect(() => {
  //   imageRef.current.draggable = false;
  // }, []);
  const preventDragHandler = (e) => e.preventDefault();

  return (
    <li onDragStart={preventDragHandler} className=" play-Card-container videoCard col-lg-3 col-md-5 col-8  px-0 ">
      <div className=" play-Card-wrapper ">
        <div className="imageCard-top">
          <picture>
            <source media="(max-width: 375px)" srcSet={images ? images.phone : ""} />

            {/* <img id="myImage" className="noSelect noEvent" src={images ? images.web : ""} alt={titleTop} /> */}
            <LazyImage imageOnload={imageOnload} src={images.web} defaultImage={placeholder} alt="placeholder" />
          </picture>
          <div ref={optionRef} className="video-card-options centerAll">
            <PlayIcon
              //  className={classNameNew}
              style={{ fontSize: "1em" }}
            />
          </div>
        </div>
        <div
          // style={titleStyle}
          className="descriptionCard-bottom noSelect"
        >
          <h4 className="play-Card-title">{titleTop}</h4>
          <span
            className="play-Card-subTitle"
            // style={{ color: themeColor ? themeColor.accentColor : "" }}
          >
            {titleMiddle}
          </span>
          <h3
            className="play-Card-text"
            //  style={{ color: themeColor ? themeColor.accentColor : "" }}
          >
            {titleBottom[0]}
            <i className="fas fa-circle" />
            {titleBottom[1]}
          </h3>
        </div>
      </div>
    </li>
  );
};

export default VideoCard;
