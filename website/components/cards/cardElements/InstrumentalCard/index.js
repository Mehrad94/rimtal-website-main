import React from "react";
// import Link from "next/link";
import placeholder from "../../../../public/assets/images/placeholder/videoplaceholder.png";

const InstrumentalCard = (props) => {
  const { titleFa, titleEn, icon, themeColor, directionWeb } = props;

  let styleColor = {
    color: themeColor.accentColor,
  };
  let style = {
    marginRight: "1.5em",
  };
  if (directionWeb === "rtl") {
    style = { marginLeft: "1.5em" };
  }
  const preventDragHandler = (e) => e.preventDefault();

  return (
    <li
      onDragStart={preventDragHandler}
      style={styleColor}
      className="box-border-Card-wrapper "
    >
      <a
        style={style}
        className="box-border-Card-container borderSolid alignCenter spaceBetween "
      >
        <div style={{ fontSize: "2em" }} className="box-border-Card-icon">
          <i style={{ color: themeColor.black }} className={icon} />
        </div>
        <div
          style={{ padding: "unset", width: "unset" }}
          className="box-border-Card-title disColumn"
        >
          <span
            className="jusCenter"
            style={{ color: styleColor.color, padding: "0.2em" }}
          >
            {titleFa}
          </span>
          <span
            className="jusCenter"
            style={{ color: themeColor.black, padding: "0.2em" }}
          >
            {titleEn}
          </span>
        </div>
      </a>
    </li>
  );
};

export default InstrumentalCard;
