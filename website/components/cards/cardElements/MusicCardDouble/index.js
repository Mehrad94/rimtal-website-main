import React, { useRef } from "react";
import PlayIcon from "../../../Ui/Icons/PlayIcon";
import HeartIcon from "../../../Ui/Icons/HeartIcon";
import DotIcon from "../../../Ui/Icons/DotIcon";
import LazyImage from "../../../../../components/LazyImage";
const MusicCardDouble = (props) => {
  const { data, themeColor, directionWeb, placeholder, dotIconClick, dotModalInfo } = props;
  // console.log({ MusicCardDoubledata: data });
  const preventDragHandler = (e) => e.preventDefault();
  const optionRef = useRef(null);
  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };
  return (
    <li onDragStart={preventDragHandler} className="disColumn doubleColum-wrapper  col-lg-2 col-md-3 col-5 px-0 ">
      {data &&
        data.map((info, index) => {
          // console.log({ info });
          return (
            <div key={"musicCard" + index} className="play-Card-container  play-Card-wrapper">
              <div className="imageCard-top">
                <picture>
                  <source media="(max-width: 375px)" srcSet={info.images.phone} />

                  {/* <img id="myImage" className="noSelect noEvent" src={info.images.web} alt={info.titleTop} /> */}
                  <LazyImage
                    imageOnload={imageOnload}
                    src={info.images.web}
                    defaultImage={placeholder}
                    alt="placeholder"
                  />
                </picture>
                <div ref={optionRef} className="card-options">
                  <a>
                    <PlayIcon
                      // className={classNameNew}
                      style={{ fontSize: "0.6em" }}
                    />
                  </a>

                  <HeartIcon style={{ fontSize: "0.6em" }} />
                  <DotIcon click={dotIconClick} modalInfo={dotModalInfo} />
                </div>
              </div>
              <div className="descriptionCard-bottom noSelect">
                <h4 className="play-Card-title">{info.titleTop}</h4>
                <span
                  className="play-Card-subTitle"
                  // style={colorStyle}
                >
                  {info.titleMiddle}
                </span>
                <h3
                  className="play-Card-text"
                  //  style={colorStyle}
                >
                  {info.titleBottom[0]}
                  <i className="fas fa-circle" />
                  {info.titleBottom[1]}
                </h3>
              </div>
            </div>
          );
        })}
    </li>
  );
};

export default MusicCardDouble;
// return <MusicCard data={info} directionWeb={directionWeb} themeColor={themeColor} key={index + "asdasd"} />;
//         })}
//     </div>
//   );
// };

// export default MusicCardDouble;
