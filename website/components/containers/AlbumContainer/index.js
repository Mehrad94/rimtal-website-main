import React, { useState } from "react";
import themeColor from "../../../values/theme/themeColor";
import Link from "next/link";
import placeholder from "../../../../public/assets/images/placeholder/albumPlaceholder.png";
import website from "../../..";
import MusicCardDouble from "../../cards/cardElements/MusicCardDouble";
import AwesomeScroll from "../../../../components/AwesomeScroll";
const modalData = [
  { icon: "far fa-stream", title: "Add To Quque" },
  { icon: "far fa-album", title: "Go To Album" },
  { icon: "fal fa-plus", title: "Add To Playlist", child: [] },
  { icon: "fas fa-share", title: "Share", child: [] },
];
const AlbumContainer = (props) => {
  const { albums } = props;

  const [state, setState] = useState({
    modal: { show: false, data: modalData },
  });
  const toggleDotIconClick = () => {
    // console.log("omad");
    setState((prev) => ({ ...prev, modal: { show: !prev.modal.show } }));
  };
  // console.log({ albums });
  const newData = website.utils.convert.albumCard(albums, 2);
  // console.log({ newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{website.values.strings.ALBUMS}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{website.values.strings.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>

      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCardDouble key={"AlbumCard-" + index} data={data} placeholder={placeholder} dotIconClick={toggleDotIconClick} dotModalInfo={state.modal} parentClass={" col-lg-2 col-md-3 col-5  px-0"} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default AlbumContainer;
