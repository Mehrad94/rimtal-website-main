import React, { useState, useEffect } from "react";
import AwesomeScroll from "../../../../components/AwesomeScroll";
import placeholder from "../../../../public/assets/images/placeholder/sliderplaceholder.png";
import website from "../../..";
import SliderCard from "../../cards/cardElements/SliderCard";

const SliderCardContainer = (props) => {
  const { sliders } = props;

  // console.log({ sliders });
  const newData = website.utils.convert.sliderCard(sliders);

  return (
    <section className="row-container row-main-wrapper">
      {" "}
      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <SliderCard key={"single-" + index} data={data} placeholder={placeholder} parentClass={" col-lg-2 col-md-3 col-5  px-0"} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default SliderCardContainer;
