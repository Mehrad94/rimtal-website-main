import React from "react";
import AwesomeScroll from "../../../../components/AwesomeScroll";
import Link from "next/link";
import themeColor from "../../../values/theme/themeColor";
import website from "../../..";
import VideoCard from "../../cards/cardElements/VideoCard";

const VideoCardContainer = (props) => {
  const { musicVideos } = props;
  // console.log({ musicVideos });

  const newData = website.utils.convert.videoCard(musicVideos);
  // console.log({ newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{website.values.strings.VIDEOS}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{website.values.strings.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>

      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <VideoCard key={"VideoCard-" + index} data={data} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default VideoCardContainer;
