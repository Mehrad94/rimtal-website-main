import React from "react";
// import "./index.scss";
const LoginElement = ({ color, strings }) => {
  return (
    <div className="login-element ">
      <div className="login-wrapper">
        <div className="login-element-container pointer" style={{ backgroundColor: color.mainColor, color: color.white }}>
          <div>
            <i className="fas fa-user-alt" />
          </div>
          <div>
            <div className="">{strings.SIGN_IN}</div>
          </div>
        </div>
        <div className="for-small-display login-element-container pointer ">
          <i className="far fa-user-circle"></i>
          <div className="chevron-down-icon">
            {" "}
            <i className="fal fa-chevron-down"></i>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginElement;
