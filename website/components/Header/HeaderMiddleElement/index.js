import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

// import "./index.scss";
const HeaderMiddleElement = ({ icon, text, color, location }) => {
  const router = useRouter();

  let locationActive = false;
  if (router.pathname === location.href) locationActive = true;
  return (
    <Link {...location}>
      <a className={`${locationActive ? "url-active-in-page" : ""} navbar-link-to-other-page`}>
        <div className="text-icon-wrapper pointer ">
          <div>
            <div className={"header-icon-middle"}>
              <i style={{ color }} className={icon} />
            </div>
            <div className={"header-text-middle "}>{text}</div>
          </div>

          <div className="active-page"></div>
        </div>
      </a>
    </Link>
  );
};

export default HeaderMiddleElement;
