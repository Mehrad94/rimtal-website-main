import React, { Fragment } from "react";
import HamburgerMenu from "../HamburgerMenu";
import logo from "../../../../public/assets/images/Rimtal.png";
import HeaderMiddleElement from "../HeaderMiddleElement";
// import consts from "../../../../public/values/consts";
import strings from "../../../values/strings";
import LoginElement from "../LoginElement";
// import "./index.scss";
import themeColor from "../../../values/theme/themeColor";
import website from "../../..";
import Link from "next/link";
import AwesomeScroll from "../../../../components/AwesomeScroll";
const Header = () => {
  return (
    <header style={{ color: themeColor.textColor }} className="header-wrapper ">
      <div className="header-container  row-main-wrapper" onDragStart={(e) => e.preventDefault()}>
        <div className="header-side-elements">
          <HamburgerMenu />
          <div className="header-search-icon centerAll pointer for-small-display">
            <i className="fal fa-search" />
          </div>
          <Link href="/">
            <a className="logo-website">
              <img src={logo} alt={"logo"} />
            </a>
          </Link>
        </div>
        <div className="logo-website for-small-display">
          <Link href="/">
            <a>
              <img src={logo} alt={"logo"} />
            </a>
          </Link>
        </div>
        <div className="header-middle-elements ">
          <AwesomeScroll>
            <div onDragStart={(e) => e.preventDefault()} className={"header-middle-into-elemen"}>
              {website.values.consts.headerMiddle.map((middle, index) => {
                return (
                  <Fragment>
                    <HeaderMiddleElement {...middle} key={"header-" + index} />
                  </Fragment>
                );
              })}
            </div>
          </AwesomeScroll>
        </div>
        <div className="header-side-elements flex-end-for-small">
          <div className="header-search-icon centerAll pointer">
            <i className="fal fa-search" />
          </div>
          <div className="header-bell-icon pointer">
            <i className="far fa-bell"></i>
          </div>
          <LoginElement strings={strings} color={themeColor} />
        </div>
      </div>
    </header>
  );
};

export default Header;
