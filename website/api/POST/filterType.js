import axios from "../axios-orders";
export const filteredData = async (filterType) =>
  axios
    .post("albums/1", {
      filters: [filterType],
    })
    .then((res) => {
      return { data: res.data };
    })
    .catch((e) => {
      return { error: e };
    });
