import strings from "../strings";

import themeColor from "../theme/themeColor";
let color = themeColor.mainColor;

const headerMiddle = [
  {
    text: strings.EXPLORE,
    icon: "far fa-compass",
    location: { href: "#" },
    color,
  },
  {
    text: strings.TRACKS,
    icon: "far fa-music",
    location: { href: "#" },
    color,
  },
  {
    text: strings.PLAYLISTS,
    icon: "far fa-list-music",
    location: { href: "#" },
    color,
  },
  {
    text: strings.ALBUMS,
    icon: "far fa-album",
    location: { href: "/albums" },
    color,
  },
  {
    text: strings.ARTISTS,
    icon: "far fa-user-music",
    location: { href: "#" },
    color,
  },
  {
    text: strings.VIDEOS,
    icon: "far fa-video",
    location: { href: "#" },
    color,
  },
];

export default headerMiddle;
