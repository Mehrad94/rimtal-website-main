import headerMiddle from "./headerMiddle";
import constJson from "./constJson";
import constJsonDouble from "./constJsonDouble";
import moreExploreConst from "./moreExploreConst";
import instrumentals from "./instrumentals";
const consts = {
  headerMiddle,
  constJson,
  constJsonDouble,
  moreExploreConst,
  instrumentals,
};
export default consts;
