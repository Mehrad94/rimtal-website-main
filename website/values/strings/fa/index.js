import navbar from "./navbar.js";
import homePage from "./homePage.js";
import global from "./global.js";
const fa = {
  ...navbar,
  ...homePage,
  ...global,
};
export default fa;
