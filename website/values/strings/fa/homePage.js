const VIEW_ALL = "مشاهده همه";
const SINGLES = "تک آهنگ ها";
const COMING_SOON = "بزودی";
const PLAY_LISTS = "لیست پخش";
const VIDEOS = "ویدیو ها";
const PLAYLIST_FOR_AUTUMN = "لیست پخش پیشنهادی ";
const MOOD = "حالت";
const COMMING_SOON = "بزودی";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  SINGLES,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON,
};
export default homePage;
