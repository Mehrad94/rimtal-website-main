const VIEW_ALL = "View All";
const SINGLES = "Singles";
const COMING_SOON = "Coming Soon";
const PLAY_LISTS = "Playlists";
const VIDEOS = "Videos";
const PLAYLIST_FOR_AUTUMN = "Playlist For Autumn";
const MOOD = "Mood";
const COMMING_SOON = "Comming Soon";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS,
  MOOD,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON,
};
export default homePage;
