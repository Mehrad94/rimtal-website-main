const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";

const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN,
};
export default navbar;
