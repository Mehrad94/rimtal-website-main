
import consts from './consts';
import strings from './strings';
import themeColor from "./theme/themeColor";
const values={consts,strings,themeColor}
export default values;