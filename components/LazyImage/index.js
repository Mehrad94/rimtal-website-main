import React, { useState, useEffect, useRef, Fragment } from "react";
export const LazyImage = ({ src, alt, defaultImage, imageOnload }) => {
  let placeHolder = defaultImage ? defaultImage : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSP-Ze14CTqFzlYouUOJ7OIGAN14YRJrmT1EwYEhsJTBrC92fhtTQ&s";

  const [imageSrc, setImageSrc] = useState(placeHolder);
  //   const [imageRef, setImageRef] = useState();
  //   console.log({ imageSrc });

  const imageRef = useRef(null);
  const onLoad = (event) => {
    // console.log({ onLoad: event });
    event.target.classList.add("loaded");
    // imageOnload();
  };

  const onError = (event) => {
    // console.log({ error: event });

    event.target.classList.add("has-error");
  };

  useEffect(() => {
    let observer;
    let didCancel = false;

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(
          (entries) => {
            // console.log({ observer, entries });

            entries.forEach((entry) => {
              if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
                setImageSrc(src);
                observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
              }
            });
          },
          {
            threshold: 0.01,
            rootMargin: "75%",
          }
        );
        observer.observe(imageRef.current);
      } else {
        // Old browsers fallback
        setImageSrc(src);
      }
    }
    return () => {
      didCancel = true;
      // on component cleanup, we remove the listner
      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]);
  //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;
  return (
    <Fragment>
      <img className="imageComponent" ref={imageRef} src={imageSrc ? imageSrc : placeHolder} alt={alt} onLoadCapture={onLoad} onError={onError} />
      <style jsx>{`
        .imageComponent {
          display: block;
        }
        .imageComponent.loaded {
          animation: loaded 0.35s ease-in-out;
        }
        .imageComponent.has-error {
          content: url(${placeHolder});
          width: 100%;
          height: 100%;
        }
        .image-lazyloading-container {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 100%;
          height: 100%;
          background-color: #ccc;
        }
        @keyframes loaded {
          0% {
            opacity: 0.7;
          }
          100% {
            opacity: 1;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default LazyImage;
